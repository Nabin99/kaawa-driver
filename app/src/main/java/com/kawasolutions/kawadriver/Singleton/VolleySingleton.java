package com.kawasolutions.kawadriver.Singleton;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.kawasolutions.kawadriver.KaawaDriverApplication;

/**
 * Created by apple on 4/21/16.
 */
public class VolleySingleton {

    private static VolleySingleton singletonInstance = null;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;
    KaawaDriverApplication application = new KaawaDriverApplication();

    private VolleySingleton(){
        mRequestQueue = Volley.newRequestQueue(application.getAppContext());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {

            private LruCache<String, Bitmap> cache = new LruCache<>((int)(Runtime.getRuntime().maxMemory()/1024/8));

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url,bitmap);
            }
        });
    }

    public static VolleySingleton getInstance(){
        if(singletonInstance == null){
            singletonInstance = new VolleySingleton();
        }
        return singletonInstance;
    }

    public  RequestQueue getRequestQueue(){
        return mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        return mImageLoader;
    }
}
