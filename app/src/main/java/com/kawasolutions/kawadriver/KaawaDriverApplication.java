package com.kawasolutions.kawadriver;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

@ReportsCrashes(
        formUri = "https://kawarides.cloudant.com/acra-kawadriver/_design/acra-storage/_update/report",
        reportType = HttpSender.Type.JSON,
        httpMethod = HttpSender.Method.POST,
        formUriBasicAuthLogin = "otheasureeseveryseepties",
        formUriBasicAuthPassword = "036ea9c0caf858fefbfc4957119511477c2c3536",
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE
        },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.toast_crash
)
public class KaawaDriverApplication extends MultiDexApplication{

    private static KaawaDriverApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
        sInstance = this;
    }

    public static KaawaDriverApplication getInstance(){
        return sInstance;
    }

    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }
}
