package com.kawasolutions.kawadriver;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by apple on 4/21/16.
 */
public class ServerRequest {

    private Context context;
    private String URL;
    private String method;
    private HashMap<String,Object> params = null;
    private RequestQueue requestQueue;

    JsonObjectRequest jsonObjectRequest;
    private ProgressDialog progressDialog;

    public ServerRequest(){
        this.context = context;
    }

    public ServerRequest(Context context, String URL, String method){
        this.context = context;
        this.URL = URL;
        this.method = method;

    }

    public ServerRequest(Context context, String URL, String method, HashMap<String, Object> params) {
        this.context = context;
        this.URL = URL;
        this.method = method;
        this.params = params;
    }

    public void sendRequest(){


        if (method == "POST") {
            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL,
                    new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                       // progressDialog.dismiss();
                    Log.d("Response:","" + response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                       // progressDialog.dismiss();
                }
            });
        }

        else {
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL,
                    new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response:","" + response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                       // progressDialog.dismiss();
                }
            });
        }

        Volley.newRequestQueue(context).add(jsonObjectRequest);
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage("Checking Credentials");
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.show();
    }
}
