package com.kawasolutions.kawadriver;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Model.DriverProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 5/16/16.
 */
public class EndRide {

    private Context context;
    private LatLng myLocation;
    private float customerRateValue;
    private ProgressDialog progressDialog;
    private String rideId;
    private CoordinatorLayout layout;
    private Snackbar snackbar;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private LinearLayout customerDetailLayout;
    private String customerName;
    private TextView customerNameRate;
    private LinearLayout ratingLayout;
    private String dist;
    private Integer minutesRidden;

    public EndRide(Context context,LatLng myLocation,String rideId,CoordinatorLayout layout,LinearLayout customerDetailLayout,String customerName,TextView customerNameRate,LinearLayout ratingLayout,String dist,Integer minutesRidden) {
        this.context = context;
        this.myLocation = myLocation;
//        this.customerRateValue = customerRateValue;
        this.rideId = rideId;
        this.layout = layout;
        this.customerDetailLayout = customerDetailLayout;
        this.customerName = customerName;
        this.customerNameRate = customerNameRate;
        this.ratingLayout = ratingLayout;
        this.dist = dist;
        this.minutesRidden = minutesRidden;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    public void endMyRide(){

        String start_latitude = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_start_ride_latitude", "");
        String start_longitude = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_start_ride_longitude", "");
        String end_latitude = String.valueOf(myLocation.latitude);
        String end_longitude = String.valueOf(myLocation.longitude);

        Log.d("Ride time:","" + minutesRidden);
        Log.d("Distance :", " " + dist);
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("rideId", Integer.valueOf(rideId));
        params.put("startLatitude", Double.valueOf(start_latitude));
        params.put("startLongitude", Double.valueOf(start_longitude));
        params.put("endLatitude", Double.valueOf(end_latitude));
        params.put("endLongitude", Double.valueOf(end_longitude));
        params.put("distance",Double.valueOf(dist));
        params.put("rideTime", minutesRidden);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.endride,
                new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("Response:", "" + response);
                try {
                    if (response != null) {

                        Integer success = 0;

                        if (response.has(Key.success)) {

                            success = (Integer) response.get(Key.success);

                            if (success == 1) {

                             ViewGroup parent = (ViewGroup) customerDetailLayout.getParent();
                                parent.removeView(customerDetailLayout);
                                if (customerName != null) {
                                 customerNameRate.setText(customerName);
                                }
                                ratingLayout.setVisibility(View.VISIBLE);

                            } else {
                                  snackbar = Snackbar.make(layout, context.getResources().getString(R.string.error), Snackbar.LENGTH_LONG);
                                    snackbar.show();
                             }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                snackbar = Snackbar.make(layout, context.getResources().getString(R.string.server_error), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                DriverProfile profile = SharedPref.getDriverObject(context,SharedPrefKey.fileName);
                String authToken = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_driver_authToken", "");
                HashMap<String, String> mHeaders = new HashMap<>();
                mHeaders.put("authToken", profile.getAuthToken());
                return mHeaders;
            }
        };
        requestQueue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

    }
}
