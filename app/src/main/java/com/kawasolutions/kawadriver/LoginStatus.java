package com.kawasolutions.kawadriver;

import android.content.Context;

import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.Utils.SharedPref;

/**
 * Created by apple on 4/22/16.
 */
public class LoginStatus {

    Context context;

    public LoginStatus(Context context){
        this.context = context;
    }

    public boolean isLoggedIn(){

        DriverProfile profile = SharedPref.getDriverObject(context, SharedPrefKey.fileName);
        if(profile!=null){
            if(profile.getLoggedIn()){
                return true;
            }
            return false;
        }
        return false;
    }
}
