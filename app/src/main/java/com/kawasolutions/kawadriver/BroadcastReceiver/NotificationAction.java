package com.kawasolutions.kawadriver.BroadcastReceiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.kawasolutions.kawadriver.Activities.RideDetails;
import com.kawasolutions.kawadriver.Service.MediaPlayerAndVibratorService;

/**
 * Created by apple on 5/5/16.
 */
public class NotificationAction extends BroadcastReceiver {

    private String customerId;
    private String rideId;
    private String destinationLatitude;
    private String destinationLongitude;

    @Override
    public void onReceive(Context context, Intent intent) {

        customerId = intent.getStringExtra("customerId");
        rideId = intent.getStringExtra("rideId");

        if(intent.getStringExtra("destinationLatitude") != null && intent.getStringExtra("destinationLongitude") != null){
            destinationLatitude = intent.getStringExtra("destinationLatitude");
            destinationLongitude = intent.getStringExtra("destinationLongitude");
        }

        if(intent.getAction().equals("Confirm")){
            stopMedia(context);
            Bundle bundle = new Bundle();
            bundle.putString("customerId",customerId);
            bundle.putString("rideId",rideId);
            if(destinationLatitude != null && destinationLongitude != null){
                bundle.putString("destinationLatitude",destinationLatitude);
                bundle.putString("destinationLongitude",destinationLongitude);
            }

            Intent rideIntent = new Intent(context, RideDetails.class);
            rideIntent.putExtras(bundle);
            rideIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(rideIntent);

        } else {

            stopMedia(context);
        }
    }

    public void stopMedia(Context context){

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Integer.valueOf(customerId));
        context.stopService(new Intent(context, MediaPlayerAndVibratorService.class));
    }

}
