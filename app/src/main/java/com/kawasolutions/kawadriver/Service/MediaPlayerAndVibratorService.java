package com.kawasolutions.kawadriver.Service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;

import com.kawasolutions.kawadriver.R;

import java.io.IOException;


public class MediaPlayerAndVibratorService extends Service {

    private MediaPlayer mMediaPlayer;
    private Vibrator vibrator;
    public MediaPlayerAndVibratorService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {

            mMediaPlayer = new MediaPlayer();

            Uri soundUri = Uri.parse("android.resource://" + getPackageName()
                    + "/" + R.raw.notification_sound);
            mMediaPlayer.setDataSource(this, soundUri);
            // Audio manager for sound setting
            final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            final int originalVolume = audioManager
                    .getStreamVolume(AudioManager.STREAM_ALARM);
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM,
                    audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM),
                    0);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                audioManager.setStreamVolume(AudioManager.STREAM_ALARM,
                        originalVolume, 0);

                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.setVolume(0.4f, 0.4f);
                mMediaPlayer.setLooping(true);
            }
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {

            mMediaPlayer.start();
        }
        if (vibrator != null) {
            vibrator.cancel();

            vibrator.vibrate(5000);
        }
        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(mMediaPlayer!=null){
            mMediaPlayer.stop();

        }
        if(vibrator==null){
            vibrator.cancel();
        }
    }

}
