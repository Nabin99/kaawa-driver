package com.kawasolutions.kawadriver.Service;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.Utils.VolleyErrorMessage;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by apple on 4/29/16.
 */
public class MyLocationService extends Service implements NetworkRequest.NetworkListener {

    private static final String TAG = "MyLocationService";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 10000;
    private static final float LOCATION_DISTANCE = 0f;

    private class LocationListener implements android.location.LocationListener {

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener: " + provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            updateLocation(location);
            Intent intent = new Intent("locationChange");
            intent.putExtra("location",location);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);

        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
            Intent intent = new Intent("locationEnabled");
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
            Intent intent = new Intent("locationDisabled");
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try{
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,LOCATION_INTERVAL,LOCATION_DISTANCE,mLocationListeners[1]);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,LOCATION_INTERVAL,LOCATION_DISTANCE,mLocationListeners[0]);
        } catch (SecurityException e){
            Log.i(TAG, "fail to request location update, ignore", e);
        } catch (IllegalArgumentException e){
            Log.i(TAG, "network provider doesnot exist", e);
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for(int i = 0; i < mLocationListeners.length; i++){
                try {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception e){
                    Log.i(TAG, "fail to remove location listener, ignore", e);
                }
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void initializeLocationManager(){
        if(mLocationManager == null){
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }


    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.UPDATE_LOCATION){
            Log.d("Driver Location update:","" + result);
            //check if driver is online or not
            try{
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1){
                            Boolean status = result.getBoolean("onlineStatus");
                            //Save driver is online or not according to server
                            SharedPref.saveToPreferences(context, SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_ONLINE,status);
                            Intent intent = new Intent("onlineStatusChange");
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            String time = result.getString("onlineTime");
                            Intent intentTime = new Intent("onlineHr");
                            intentTime.putExtra("onlineHrs",time);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentTime);
                        }
                    }
                }

            }catch (Exception e){
                Log.d("ExceptionDriver:" ,"" + e.toString());
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.UPDATE_LOCATION){
            Intent intent = new Intent("internetDisconnected");
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
        Log.d("Driver Location update:","" + VolleyErrorMessage.handleVolleyErrors(context,error));
    }

    public void updateLocation(Location location) {

        Double lat = location.getLatitude();
        Double longitude = location.getLongitude();

        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if (driverProfile != null) {
            Integer driverID = driverProfile.getDriverId();

            HashMap<String, Object> params = new HashMap<>();
            params.put("driverId", driverID);
            params.put("latitude", lat);
            params.put("longitude", longitude);

            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", driverProfile.getAuthToken());
            NetworkRequest networkRequest = new NetworkRequest(this, URLS.updateLocation, Request.Method.POST, TYPEREQUEST.UPDATE_LOCATION, params, mHeaders, true,"",null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }
    }
}
