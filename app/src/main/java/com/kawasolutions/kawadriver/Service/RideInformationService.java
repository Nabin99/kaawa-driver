package com.kawasolutions.kawadriver.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.kawasolutions.kawadriver.Activities.MainActivity;
import com.kawasolutions.kawadriver.Activities.RideDetails;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.Model.RideActivity;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by apple on 5/8/17.
 */

public class RideInformationService extends Service implements NetworkRequest.NetworkListener {

    private Timer timer;
    private String TAG="RideInfoServiceClass";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,"ON BIND");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int rideId = intent.getExtras().getInt("rideId");
        Log.d(TAG,"On Start" + rideId);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                sendRideInformationRequest(rideId);
            }
        }, 0, 15000);
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    public void sendRideInformationRequest(int rideId){
        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if(driverProfile != null) {
            String url = URLS.rideActivityStatus + rideId;
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", driverProfile.getAuthToken());
            NetworkRequest networkRequest = new NetworkRequest(this, url, Request.Method.GET, TYPEREQUEST.RIDEACTIVITYSTATUS, null, mHeaders,true, "",null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest.equals(TYPEREQUEST.RIDEACTIVITYSTATUS)){
            try {
                JSONObject rideData = result.getJSONObject("currentRideDetails");

                Double newDriverLatitude = rideData.getDouble("driverLatitude");
                Double newDriverLongitude = rideData.getDouble("driverLongitude");
                Boolean startRideStatus = rideData.getBoolean("startRideStatus");
                Boolean successStatus = rideData.getBoolean("successStatus");
                Boolean cancelledStatus = rideData.getBoolean("cancelledStatus");
                Boolean activeRideStatus = rideData.getBoolean("activeRideStatus");
                Boolean expireStatus = rideData.getBoolean("expireStatus");


                RideActivity rideActivity = RideActivity.getInstance();
                rideActivity.setDriverLocation(new LatLng(newDriverLatitude,newDriverLongitude));
                rideActivity.setRideStarted(startRideStatus);
                rideActivity.setRideSuccess(successStatus);
                rideActivity.setRideCancelled(cancelledStatus);
                rideActivity.setRideActive(activeRideStatus);
                rideActivity.setExpireStatus(expireStatus);

                updateUIFromRideActivity(rideActivity);

            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
    }

    public void updateUIFromRideActivity(RideActivity rideActivity){
        if(rideActivity.getExpireStatus()){
            stopSelf();
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,0);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,"");
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,0);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_METER_PRICE,"");

            showNotification(getResources().getString(R.string.rideExpiredText));
        } else if(rideActivity.getRideCancelled()){
            stopSelf();
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,0);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,"");
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,0);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_METER_PRICE,"");
            //showNotification("Your ride has been cancelled");
        }
         else if(rideActivity.getRideSuccess()){
            stopSelf();
            //Save rate Request status to Shared pref
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,true);
            DriverProfile profile = SharedPref.getDriverObject(this,SharedPrefKey.fileName);
            if(profile != null){
                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,profile.getDriverId());
            }
        }
        else if(rideActivity.getRideStarted()){
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,true);
            DriverProfile profile = SharedPref.getDriverObject(this,SharedPrefKey.fileName);
            if(profile != null){
                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,profile.getDriverId());
            }
        }
        Intent i = new Intent("rideInformation");
        Gson gson = new Gson();
        i.putExtra("rideDetails", gson.toJson(rideActivity));
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
    }


    public void showNotification(String message){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.taxi_marker)
                .setLargeIcon(getLargeTaxiIcon())
                .setContentTitle("Kawa")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private Bitmap getLargeTaxiIcon(){
        return BitmapFactory.decodeResource(getResources(),R.drawable.taxi_marker);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(timer != null){
            timer.cancel();
            timer.purge();
        }
    }
}
