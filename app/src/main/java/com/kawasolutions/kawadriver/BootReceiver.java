package com.kawasolutions.kawadriver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.kawasolutions.kawadriver.Activities.Home;
import com.kawasolutions.kawadriver.Service.MyLocationService;

/**
 * Created by apple on 5/10/17.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Boot Received", Toast.LENGTH_SHORT).show();
        context.startService(new Intent(context, MyLocationService.class));
    }
}
