package com.kawasolutions.kawadriver.Enum;

/**
 * Created by nabin on 1/27/17.
 */

public enum TYPEREQUEST {
    LOGIN,
    UPDATE_LOCATION,
    CHECK_ACCEPT_STATUS,
    START_RIDE,
    END_RIDE,
    UPDATE_CUSTOMER_RATING,
    TOKEN_REGISTRATION,
    CHANGE_DRIVER_ONLINE_STATUS,
    RIDE_HISTORY,
    DRIVER_ACCOUNT,
    RIDE_ACTIVITY_STATUS,
    CHECK_APP_VERSION,
    DRIVER_DECLINED,
    ADD_APP_VERSION,
    SAVE_REQUEST_COUNT,
    NEARBYDRIVERS,
    SEND_NOTIFICATION,
    UPDATE_DRIVER_RATING,
    RIDEACTIVITYSTATUS,
    CANCEL_RIDE,
    LAST_TRIPS,
    UNRATED_LAST_TRIPS,
    PROFILE_IMAGE,
    FORGOT_PASSWORD,
    VERIFY_NUMBER,
    EMERGENCY,
    LOG_OUT
}
