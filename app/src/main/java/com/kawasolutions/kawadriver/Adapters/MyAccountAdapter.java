package com.kawasolutions.kawadriver.Adapters;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kawasolutions.kawadriver.Model.MyAccount;
import com.kawasolutions.kawadriver.R;

import java.util.ArrayList;

/**
 * Created by apple on 6/29/16.
 */
public class MyAccountAdapter extends RecyclerView.Adapter<MyAccountAdapter.MyViewHolder> {

    private ArrayList<MyAccount> myAccountArrayList;

    public MyAccountAdapter(ArrayList<MyAccount> myAccountArrayList) {
        this.myAccountArrayList = myAccountArrayList;
    }

    public void setMyAccountArrayList(ArrayList<MyAccount> myAccountArrayList){
        if(myAccountArrayList != null) {
            this.myAccountArrayList = myAccountArrayList;
            notifyItemRangeChanged(0, myAccountArrayList.size());
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView date,ride,amount,paidAmount,dues;
        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            ride = (TextView) itemView.findViewById(R.id.ride);
            amount = (TextView) itemView.findViewById(R.id.amount);
            paidAmount = (TextView) itemView.findViewById(R.id.paid);
            dues = (TextView) itemView.findViewById(R.id.dues);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_account_row,parent,false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyAccount myAccount = myAccountArrayList.get(position);
        Log.d("myAccount","" + myAccount.toString());
        holder.date.setText(convertDate(myAccount.getPaymentDate(),"yyyy-MM-dd"));
        holder.ride.setText(myAccount.getRideCount().toString());
        holder.amount.setText(myAccount.getTotalAmount().toString());
        holder.paidAmount.setText(myAccount.getPaidAmount().toString());
        holder.dues.setText(myAccount.getDue().toString());
    }

    @Override
    public int getItemCount() {
        return myAccountArrayList.size();
    }


    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }
}
