package com.kawasolutions.kawadriver.Adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.Model.RideHistory;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by apple on 5/24/16.
 */
public class RideHistoryAdapter  extends RecyclerView.Adapter<RideHistoryAdapter.MyViewHolder>{

    private ArrayList<RideHistory> rideHistoryList;
    VolleySingleton volleySingleton = VolleySingleton.getInstance();
    ImageLoader imageLoader = volleySingleton.getImageLoader();

    public RideHistoryAdapter(ArrayList<RideHistory> rideHistoryList) {
        this.rideHistoryList = rideHistoryList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView customerName,rideDate,rideStatus,startLocation,endLocation,priceBreakdown;
        private CircleImageView customerImageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            customerName = (TextView) itemView.findViewById(R.id.customer_name);
            rideDate = (TextView) itemView.findViewById(R.id.date);
            rideStatus = (TextView) itemView.findViewById(R.id.ride_status);
            startLocation = (TextView) itemView.findViewById(R.id.start_location);
            endLocation = (TextView) itemView.findViewById(R.id.end_location);
            customerImageView = (CircleImageView) itemView.findViewById(R.id.driver_image);
            priceBreakdown = (TextView) itemView.findViewById(R.id.price_breakdown);
        }
    }

    @Override
    public RideHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ride_history_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RideHistoryAdapter.MyViewHolder holder, int position) {
        RideHistory rideHistory = rideHistoryList.get(position);
        holder.customerName.setText(rideHistory.getCustomerName());
        holder.rideDate.setText(convertDate(rideHistory.getDate(),"yyyy-MM-dd hh:mm a"));
        if(rideHistory.getStartLocation() != null){
            String startLoc = "<font color = '#388E3C'>&#8226;</font>" + rideHistory.getStartLocation();
            holder.startLocation.setText(Html.fromHtml(startLoc));
        }else {
            holder.startLocation.setText("");
        }
        if(rideHistory.getEndLocation() != null){
            String endLoc = "<font color = '#b12512'>&#8226;</font>" + rideHistory.getEndLocation();
            holder.endLocation.setText(Html.fromHtml(endLoc));
        } else {
            holder.endLocation.setText("");
        }

        if(rideHistory.getCancelStatus().equals("true")){
            String text = "<font color = '#BF360C'> Cancelled </font>";
            holder.rideStatus.setText(Html.fromHtml(text));
            holder.priceBreakdown.setText(null);
        }
        else if(rideHistory.getStatus().equals("true")){
            if(rideHistory.getPrice() != null){
                String price = "Rs " + (rideHistory.getPrice() + rideHistory.getExtendedPrice());
                holder.rideStatus.setText(price);
                holder.priceBreakdown.setText(rideHistory.getPrice() + " + " + rideHistory.getExtendedPrice());

            } else {
                holder.rideStatus.setText("Completed");
            }
        } else {
            holder.rideStatus.setText("Not Completed");
            holder.rideStatus.setText(null);
        }

        if(rideHistory.getImageName() != null) {
            String imageURL = URLS.driverImageURL + rideHistory.getImageName();
            imageLoader.get(imageURL, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.customerImageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.customerImageView.setImageResource(R.drawable.profile_image_boy);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return rideHistoryList.size();
    }

    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }


}
