package com.kawasolutions.kawadriver;

import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;

import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Utils.SharedPref;

import org.w3c.dom.Text;

/**
 * Created by apple on 5/16/16.
 */
public class UpdateTime {

    private TextView timer;
    private TextView distance;
    private float distanceMeasured;
    long starttime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedtime = 0L;
    int t = 1;
    int secs = 0;
    int mins = 0;
    int hr = 0;
    int km = 0;
    int m = 0;
    Handler handler;
    Integer minutes;
    Boolean isTimerRunning = false;

    public UpdateTime(TextView timer,TextView distance,float distanceMeasured) {
        this.timer = timer;
        this.distance = distance;
        this.distanceMeasured = distanceMeasured;
        handler = new Handler();
    }

    public void startTime(){
        if(!isTimerRunning) {
            starttime = SystemClock.uptimeMillis();
            handler.postDelayed(updateTimer, 0);
        }
    }

    public Runnable updateTimer = new Runnable() {
        public void run() {
            isTimerRunning = true;
            timeInMilliseconds = SystemClock.uptimeMillis() - starttime;
            updatedtime = timeSwapBuff + timeInMilliseconds;
            secs = (int) (updatedtime / 1000);
            mins = secs / 60;
            minutes = mins;
            secs = secs % 60;
            hr =  (mins / 60);
            timer.setText("" + hr + ":" + String.format("%02d", mins) + ":"
                    + String.format("%02d", secs));
            handler.postDelayed(this, 0);
        }};

    public void updateDistance(){
        if (distanceMeasured != 0){
            km = (int)distanceMeasured / 1000;
            m = (int)distanceMeasured % 1000;
            distance.setText("" + km + ":" + String.format("%04d",m));
        }
    }

    public void stopRunnable(){
        handler.removeCallbacks(updateTimer);
    }

    public Integer getMinutes(){
        return minutes;
    }

}
