package com.kawasolutions.kawadriver.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Model.DriverProfile;

/**
 * Created by apple on 4/22/16.
 */
public class SharedPref {

    public static void saveToPreferences(Context context, String filename, String preferenceName,@Nullable String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName,preferenceValue);
        editor.apply();
    }

    public static void saveToPreferences(Context context, String filename, String preferenceName,Boolean preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(preferenceName,preferenceValue);
        editor.apply();
    }

    public static void saveToPreferences(Context context, String filename, String preferenceName,Integer preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(preferenceName,preferenceValue);
        editor.apply();
    }

    public static void saveToPreferences(Context context, String filename, String preferenceName,Float preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(preferenceName,preferenceValue);
        editor.apply();
    }

    public static String readFromPreference(Context context, String filename, String preferenceName , String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName,defaultValue);
    }

    public static Boolean readBooleanFromPreference(Context context, String filename, String preferenceName , Boolean defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(preferenceName,defaultValue);
    }

    public static Integer readIntegerFromPreference(Context context, String filename, String preferenceName){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(preferenceName,0);
    }
    public static Float readFloatFromPreference(Context context, String filename, String preferenceName){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        return sharedPreferences.getFloat(preferenceName,0);
    }


    public static DriverProfile getDriverObject(Context context, String filename){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(SharedPrefKey.KEY_DRIVER_PROFILE, "");
        DriverProfile obj = gson.fromJson(json, DriverProfile.class);
        return obj;
    }
}
