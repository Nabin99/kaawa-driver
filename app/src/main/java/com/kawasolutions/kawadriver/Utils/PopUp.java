package com.kawasolutions.kawadriver.Utils;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import com.kawasolutions.kawadriver.Activities.CurrentRideActivity;
import com.kawasolutions.kawadriver.Activities.DriverRuleActivity;
import com.kawasolutions.kawadriver.Activities.Home;
import com.kawasolutions.kawadriver.Activities.MainActivity;
import com.kawasolutions.kawadriver.Activities.MyAccountActivity;
import com.kawasolutions.kawadriver.Activities.RateCardActivity;
import com.kawasolutions.kawadriver.Activities.RideHistoryActivity;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.CustomToast;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.Enum.ToastType;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.Service.MyLocationService;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by apple on 4/22/16.
 */
public class PopUp implements NetworkRequest.NetworkListener {

    private ProgressDialog progressDialog;
    private Snackbar snackbar;
    private Context context;
    private LocationManager locationManager;
    private LocationListener locationListener;

    public PopUp(Context context, LocationManager locationManager, LocationListener locationListener) {
        this.context = context;
        this.locationListener = locationListener;
        this.locationManager = locationManager;
    }

    public void popUpMenu(int id) {

        switch (id) {
//            case R.id.log_out:
//                logout();
//                break;

            case R.id.ride_history:
                showRideHistory();
                break;
            case R.id.my_account:
                showDriverAccount();
                break;
//            case R.id.current_ride:
//                showCurrentRide();
//                break;
            case R.id.rate_card:
                showRateCard();
                break;

            case R.id.driver_rule:
                showRuleBook();
                break;
            default:
                break;

        }
    }


    public void logout() {

        DriverProfile driverProfile = SharedPref.getDriverObject(context, SharedPrefKey.fileName);
        if (driverProfile != null) {
            String url = URLS.logout + driverProfile.getDriverId();
            NetworkRequest networkRequest = new NetworkRequest(context, url, Request.Method.GET, TYPEREQUEST.LOG_OUT, null, null, false,"Logging out...",null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }

    }

    public void showRideHistory(){
       context.startActivity(new Intent(context, RideHistoryActivity.class));
    }

    public void showDriverAccount() {
        context.startActivity(new Intent(context, MyAccountActivity.class));
    }

    public void showCurrentRide() {
        context.startActivity(new Intent(context, CurrentRideActivity.class));
    }

    public void showRateCard(){
        context.startActivity(new Intent(context, RateCardActivity.class));
    }

    public void showRuleBook(){
        context.startActivity(new Intent(context, DriverRuleActivity.class));
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.LOG_OUT){
            try {
                if (result != null) {
                    if (result.has(Key.success)) {
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1) {
                            // Removes location updates after driver Logs Out
                            SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_DRIVER_PROFILE, "");

                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            context.stopService(new Intent(context, MyLocationService.class));

                            context.startActivity(new Intent(context, MainActivity.class));
                            ((Home) context).finish();
                        } else {
                            CustomToast.getInstance().showToast(context,"Something went wrong....", ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        CustomToast.getInstance().showToast(context,VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR,Toast.LENGTH_SHORT);
    }
}
