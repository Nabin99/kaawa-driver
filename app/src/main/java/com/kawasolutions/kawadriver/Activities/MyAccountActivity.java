package com.kawasolutions.kawadriver.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.kawasolutions.kawadriver.DividerItemDecoration;
import com.kawasolutions.kawadriver.Adapters.MyAccountAdapter;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.CustomToast;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.Enum.ToastType;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.Model.MyAccount;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyAccountActivity extends AppCompatActivity implements NetworkRequest.NetworkListener{

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private TextView ride_count;
    private TextView rate_count;
    private TextView amount_count;
    private TextView driver_name;
    private TextView driver_phone;
    private TextView driver_online_hrs;

    private RecyclerView recyclerView;
    private MyAccountAdapter myAccountAdapter;
    private ArrayList<MyAccount> myAccountArrayList = new ArrayList<>();
    private TextView textView;

    private CircleImageView profileImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        imageLoader = volleySingleton.getImageLoader();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        driver_name = (TextView) findViewById(R.id.username);

        driver_phone = (TextView) findViewById(R.id.phone);
        ride_count = (TextView) findViewById(R.id.ride_count);
        rate_count = (TextView) findViewById(R.id.rate_count);
        amount_count = (TextView) findViewById(R.id.amount);
        driver_online_hrs = (TextView) findViewById(R.id.online_hrs);

        textView = (TextView) findViewById(R.id.textView);
        profileImageView = (CircleImageView) findViewById(R.id.profile_image);

        LinearLayoutManager layoutParams = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutParams);
        myAccountAdapter = new MyAccountAdapter(myAccountArrayList);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(myAccountAdapter);

        getDriverAccount();
    }


    public void getDriverAccount() {

        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if (driverProfile != null) {
            HashMap<String,String> mHeaders = new HashMap<>();
            mHeaders.put("authToken",driverProfile.getAuthToken());

            String url = URLS.driverAccount + driverProfile.getDriverId();

            NetworkRequest networkRequest = new NetworkRequest(this, url, Request.Method.GET, TYPEREQUEST.DRIVER_ACCOUNT, null, mHeaders,false, getResources().getString(R.string.pleaseWaitText),null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }

    }

    public void parseJSONResponse(JSONObject response) {
        try {

            JSONObject jsonObject = response.getJSONObject("driverProfile");
            String averageRating = jsonObject.getString("averageRating");
            String imageName = jsonObject.getString("image");
            String first_Name = Character.toUpperCase(jsonObject.getString("firstName").charAt(0)) + jsonObject.getString("firstName").substring(1);
            String last_Name = Character.toUpperCase(jsonObject.getString("lastName").charAt(0)) + jsonObject.getString("lastName").substring(1);
            Integer totalRideCount = jsonObject.getInt("rideCount");
            String driverName = first_Name + " " + last_Name;

            String driverPhone = jsonObject.getString("contactNo");
            String driverStatus = jsonObject.getString("availableStatus");
            String driverImage = jsonObject.getString("image");
            String onlineHr = response.getString("driverOnlineToday");

            showImage(driverImage);

            Integer rideCount;
            Double amount = 0.0;
            Double dues = 0.0;
            JSONObject jsonObjectPayment = response.getJSONObject("paymentBill");
                rideCount = jsonObjectPayment.getInt("totalRides");
                amount = jsonObjectPayment.getDouble("amount");
                dues = jsonObjectPayment.getDouble("due");


            rate_count.setText(averageRating);
            ride_count.setText(totalRideCount.toString());
            if(amount != null && dues != null) {
                amount_count.setText(amount + dues + "");
            }
            driver_name.setText(driverName);
            driver_phone.setText(driverPhone);
            driver_online_hrs.setText(onlineHr);
//            if (driverStatus.equals("true")) {
//                status = "Available";
//                driver_status_option.setText(status);
//
//            } else {
//                status = "Unavailable";
//                driver_status_option.setText(status);
//            }

            JSONArray accountArray = response.getJSONArray("driverAccounts");
            for(int i=0; i< accountArray.length();i++){
                JSONObject object = accountArray.getJSONObject(i);
                MyAccount myAccount = new MyAccount();
                myAccount.setPaymentDate(object.getString("date"));
                myAccount.setRideCount(object.getInt("totalRides"));
                myAccount.setTotalAmount(object.getDouble("amount"));
                myAccount.setPaidAmount(object.getDouble("paid"));
                myAccount.setDue(object.getDouble("dues"));

                myAccountArrayList.add(myAccount);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, Home.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void showImage(String imageName){
            String imageURL =URLS.driverImageURL + imageName;

        imageLoader.get(imageURL, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                profileImageView.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if(((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap() != null) {
                    Bitmap icon = ((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap();
                    profileImageView.setImageBitmap(icon);
                }
            }
        });
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        Log.d("Driver account :","" + result);
        if(typeRequest == TYPEREQUEST.DRIVER_ACCOUNT){
            try {
                if (result != null) {
                    if (result.has(Key.success)) {
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1) {
                            parseJSONResponse(result);
                            myAccountAdapter.setMyAccountArrayList(myAccountArrayList);
                            if(myAccountArrayList.size() == 0){
                                textView.setText(getResources().getString(R.string.noPaymentHistoryText));
                            } else {
                                textView.setText("");
                            }
                        } else {
                            CustomToast.getInstance().showToast(context,getResources().getString(R.string.noProfileText), ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error),ToastType.ERROR,Toast.LENGTH_SHORT);
    }
}


