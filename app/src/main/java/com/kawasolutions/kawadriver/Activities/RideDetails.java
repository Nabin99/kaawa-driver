package com.kawasolutions.kawadriver.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Text;
import com.google.gson.Gson;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.CustomToast;
import com.kawasolutions.kawadriver.Enum.ToastType;
import com.kawasolutions.kawadriver.Model.RideInfo;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.MapFunction;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.Model.RideActivity;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Service.RideInformationService;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.UpdateTime;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class RideDetails extends AppCompatActivity implements OnMapReadyCallback,LocationListener,NetworkRequest.NetworkListener {

    private ProgressDialog progressDialog;
    private SupportMapFragment supportMapFragment;
    private GoogleMap map;
    private Snackbar snackbar;
    private TextView name;
    private TextView phone;
    private TextView timer;
    private TextView distance;
    private ImageButton callCustomerBtn;
    private LinearLayout customerDetailLayout;
    private LinearLayout endRideLayout;
    private Button startRideBtn;
    private CoordinatorLayout layout;
    private Button endRideBtn;
    private LocationManager locationManager;
    private LinearLayout ratingLayout;
    private TextView customerNameRate;
    private Button rateButton;
    private RatingBar ratingBar;
    private float customerRateValue;
    private TextView customerRating;

    private Marker myLocationMarker;
    private LatLng myLocation;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private boolean distanceCalculation = false; // for starting distance calculation only after start ride btn is pressed
    private float distanceMeasured = 0;
    private Location myLocationClone;
    private UpdateTime updateTime;
    private Location newLocation;
    private CameraPosition cameraPosition;
    public static boolean active = true;
    private Integer minutesRidden = 0;
    private String dist = "0";
    private CircleImageView profileImageCustomer;
    private CircleImageView profileImageCustomer2;
    private RideInfo rideInfo;
    private Marker customerMarker;
    private Marker destinationMarker;
    private Boolean activityRelaunced = false;
    private Chronometer chronometer;
    public static Timer rideActivitiesTimer;
    private CardView cardView;
    private TextView extendedRangeText;
    private TextView extraCharge;

    //RAting page
    private TextView subTotal;
    private TextView finalTotal;
    private Double serviceCharge = 20.0;
    private Boolean extendedRange =false;
    private Double extendedRangePrice = 0.0;
    private Double driverTypedMarketPrice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);

        setAllLayouts();

        String rideDetailsString = getIntent().getExtras().getString("rideDetails");
        Boolean activityLaunced = getIntent().getExtras().getBoolean("activityRelaunced");
        if(activityLaunced){
            activityRelaunced = true;
        }

        Gson gson = new Gson();
        rideInfo = gson.fromJson(rideDetailsString,RideInfo.class);

        updateTime = new UpdateTime(timer, distance, distanceMeasured);

        //check if ride is extended range
        if(rideInfo.getExtendedRange()){
            cardView.setVisibility(View.VISIBLE);
            extendedRangeText.setText(getResources().getString(R.string.extraChargeText) + "Rs " + rideInfo.getExtendedRangePrice());
        }
        hideAllLayout();
        checkPreviousState();

        supportMapFragment.getMapAsync(this);    // initialize map : this calls OnMapReady Method

        callCustomerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rideInfo != null) {
                    if (rideInfo.getCustomer().getContactNo() != null) {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + rideInfo.getCustomer().getContactNo()));
                        startActivity(callIntent);
                    }
                }
            }
        });


        // start the ride after customer is inside the taxi
        startRideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOTPPromptDialog();
            }
        });

        // show rating layout after reaching the destination
        endRideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    showPricePromptDialog();
            }
        });

        // get customer rating after ending the ride
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rateButton.getVisibility() == View.INVISIBLE) {
                    rateButton.setVisibility(View.VISIBLE);
                }
                customerRateValue = rating;

            }
        });


        //End ride AFter rating customer
        rateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DriverProfile driverProfile = SharedPref.getDriverObject(RideDetails.this, SharedPrefKey.fileName);
                if (driverProfile != null) {
                    if (rideInfo != null) {
                        HashMap<String, Object> params = new HashMap<String, Object>();
                        params.put("rideId", rideInfo.getRideId());
                        params.put("customerRating", customerRateValue);

                        HashMap<String,String> mHeaders = new HashMap<>();
                        mHeaders.put("authToken",driverProfile.getAuthToken());

                        NetworkRequest networkRequest = new NetworkRequest(RideDetails.this, URLS.updateCustomerRating, Request.Method.POST, TYPEREQUEST.UPDATE_CUSTOMER_RATING, params, mHeaders,false, getResources().getString(R.string.ratingCustomerText),null);
                        networkRequest.setListener(RideDetails.this);
                        networkRequest.getResult();
                    }
                }
            }
        });

        Intent i = new Intent(this,RideInformationService.class);
        i.putExtra("rideId",rideInfo.getRideId());
        startService(i);
    }

    public void checkRideActivitiesStatus(){
        rideActivitiesTimer = new Timer();
        rideActivitiesTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d("Ride Info:", "Timer for Ride Activities Started");
                runOnUiThread(new Runnable() {
                    public void run() {
                        getRideActivitiesFromNetwork();
                    }
                });
            }
        }, 0, 20000);
    }

    public void getRideActivitiesFromNetwork(){
        DriverProfile driverProfile = SharedPref.getDriverObject(RideDetails.this, SharedPrefKey.fileName);
        if(driverProfile != null) {
            String url = URLS.rideActivityStatus + rideInfo.getRideId();
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", driverProfile.getAuthToken());
            NetworkRequest networkRequest = new NetworkRequest(RideDetails.this, url, Request.Method.GET, TYPEREQUEST.RIDE_ACTIVITY_STATUS, null, mHeaders,true, "",null);
            networkRequest.setListener(RideDetails.this);
            networkRequest.getResult();
        }
    }

    public void endRide(String price){
        distanceCalculation = false;
        //stop Runnable i.e time calculated
        updateTime.stopRunnable();
        //chronometer.stop();

        int stoppedMilliseconds = 0;

//        String chronoText = chronometer.getText().toString();
//        String array[] = chronoText.split(":");
//        if (array.length == 2) {
//            stoppedMilliseconds = Integer.parseInt(array[0]) * 60 * 1000
//                    + Integer.parseInt(array[1]) * 1000;
//        } else if (array.length == 3) {
//            stoppedMilliseconds = Integer.parseInt(array[0]) * 60 * 60 * 1000
//                    + Integer.parseInt(array[1]) * 60 * 1000
//                    + Integer.parseInt(array[2]) * 1000;
//        }
//        minutesRidden = stoppedMilliseconds/60000;
        SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_METER_PRICE,price);
        if(myLocation != null) {
            DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
            if (driverProfile != null) {
                if (rideInfo != null) {
                    HashMap<String, Object> params = new HashMap<String, Object>();
                    params.put("rideId", rideInfo.getRideId());
                    params.put("endLatitude", myLocation.latitude);
                    params.put("endLongitude", myLocation.longitude);
                    params.put("distance",Double.valueOf(dist));
                    params.put("rideTime",minutesRidden);
                    params.put("meterPrice",Double.valueOf(price));

                    HashMap<String,String> mHeaders = new HashMap<>();
                    mHeaders.put("authToken",driverProfile.getAuthToken());

                    NetworkRequest networkRequest = new NetworkRequest(this, URLS.endride, Request.Method.POST, TYPEREQUEST.END_RIDE, params, mHeaders,false, getResources().getString(R.string.endingRideText),null);
                    networkRequest.setListener(this);
                    networkRequest.getResult();
                }
            }
        } else {
            CustomToast.getInstance().showToast(this,getResources().getString(R.string.enableGPSAlertText), ToastType.INFO,Toast.LENGTH_SHORT);
        }
    }

    public void showCustomerDetailsToDriver(){
        if(rideInfo != null){
            if(rideInfo.getCustomer().getImageName() != null){
                showCustomerImage(rideInfo.getCustomer().getImageName());
            }
            name.setText(rideInfo.getCustomer().getFirstName() + " " + rideInfo.getCustomer().getLastName());
            phone.setText(rideInfo.getCustomer().getContactNo());
            if(rideInfo.getCustomer().getRating() != null){
                customerRating.setText(String.valueOf(rideInfo.getCustomer().getRating()));
            } else {
                customerRating.setText("ND*");
            }
            setCustomerMarker(rideInfo.getCustomerLocation());
            if(rideInfo.getDestinationLocation() != null){
                setDestinationMarker(rideInfo.getDestinationLocation());
            }
            customerDetailLayout.setVisibility(View.VISIBLE);
        } else {
            Log.d("Ride Details:","Ride info null");
        }
    }

    public void setCustomerMarker(LatLng location){
        customerMarker = map.addMarker(new MarkerOptions() .title(getResources().getString(R.string.customerLocationText)) .position(location));
        customerMarker.setIcon(BitmapDescriptorFactory.defaultMarker());
    }

    public void setDestinationMarker(LatLng location){
        destinationMarker = map.addMarker(new MarkerOptions() .title(getResources().getString(R.string.destinationText)) .position(location));
        destinationMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        requestLocationUpdate();
                    } else {
                        configureLocation();
                    }
                } else {
                    configureLocation();
                }

        }
    }

    public void configureLocation(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);
            } else {
                requestLocationUpdate();
            }
        } else {
            requestLocationUpdate();
        }
    }

    private void requestLocationUpdate() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 7000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 7000, 0, this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(Key.DefaultCoordinates, 12));
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        showCustomerDetailsToDriver();
        configureLocation();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("app status details:", "ON PAUSE");
        active = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(cancelEventReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(rideInformationReceiver);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("app status details:", "ON RESUME");
        active = true;
        LocalBroadcastManager.getInstance(this).registerReceiver(cancelEventReceiver,
                new IntentFilter("cancel-event"));
        LocalBroadcastManager.getInstance(this).registerReceiver(rideInformationReceiver,
                new IntentFilter("rideInformation"));
        checkPreviousState();
    }

    private void setRideDistance(){
        Float savedDistance = SharedPref.readFloatFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DISTANCE);
        Log.d("Saved Distance:", "" + savedDistance);

        distanceMeasured =  savedDistance;
        dist = String.format("%.2f", distanceMeasured / 1000);
        distance.setText(dist + " km");
    }

    @Override
    public void onBackPressed() {
    }

    public BroadcastReceiver cancelEventReceiver  = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getExtras().getString("message");

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RideDetails.this);
            alertDialogBuilder.setTitle(getResources().getString(R.string.rideCancelledTitle));
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.okText), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent intent = new Intent(RideDetails.this, Home.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });


            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    };

    public void hideAllLayout(){
        customerDetailLayout.setVisibility(View.INVISIBLE);
        endRideLayout.setVisibility(View.INVISIBLE);
        ratingLayout.setVisibility(View.INVISIBLE);
    }

    public void checkPreviousState(){
        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if(driverProfile != null) {
            Boolean rideOn = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_ON_RIDE, false);
            if (rideOn) {
                Boolean rateRequest = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_RATE_REQUEST, false);
                Boolean rideStarted = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_RIDE_STARTED, false);

                Integer rateDriverId = SharedPref.readIntegerFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID);
                Integer rideDriverId = SharedPref.readIntegerFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID);

                if (rateRequest && rateDriverId.equals(driverProfile.getDriverId())) {
                    ratingLayout.setVisibility(View.VISIBLE);
                    customerDetailLayout.setVisibility(View.INVISIBLE);
                    endRideLayout.setVisibility(View.INVISIBLE);
                    if(rideInfo != null){
                        customerNameRate.setText(rideInfo.getCustomer().getFirstName() + " " + rideInfo.getCustomer().getLastName());
                    }
                    String meterPrice =  SharedPref.readFromPreference(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_METER_PRICE,"");
                    if(!meterPrice.equals("")){
                        subTotal.setText(meterPrice);
                        Double price = Double.valueOf(meterPrice);
                        Double total = price + serviceCharge;
                        finalTotal.setText("Rs " + String.valueOf(total));
                    }
                    if(activityRelaunced){
                        CustomToast.getInstance().showToast(this,getResources().getString(R.string.rateCustomerText),ToastType.INFO,Toast.LENGTH_SHORT);
                    }
                }
                else if (rideStarted && rideDriverId.equals(driverProfile.getDriverId())) {
                    endRideLayout.setVisibility(View.VISIBLE);
                    ratingLayout.setVisibility(View.INVISIBLE);
                    customerDetailLayout.setVisibility(View.INVISIBLE);
                    //setRideDistance();
                    updateTime.startTime();
                    distanceCalculation = true;
                    if(activityRelaunced){
                        CustomToast.getInstance().showToast(this,getResources().getString(R.string.rideNotComplete),ToastType.INFO,Toast.LENGTH_SHORT);
                    }
                } else {
                    customerDetailLayout.setVisibility(View.VISIBLE);
                    endRideLayout.setVisibility(View.INVISIBLE);
                    ratingLayout.setVisibility(View.INVISIBLE);
                }
            } else {
                Intent i = new Intent(RideDetails.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        }
    }

    public void setChronometer(){
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int)(time/3600000);
                int m = (int)(time - h*3600000)/60000;
                int s = (int)(time - h*3600000 - m*60000)/1000;
                String hh = h < 10 ? "0"+h: h+"";
                String mm = m < 10 ? "0"+m: m+"";
                String ss = s < 10 ? "0"+s: s+"";
                chronometer.setText(hh+":"+mm+":"+ss);
            }
        });
        chronometer.start();
    }

    public void showOTPPromptDialog(){
        //get OTP code view
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.opt_code_view,null);
        final EditText otpTextView = (EditText) promptView.findViewById(R.id.otpEditText);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setView(promptView);

        //setup a dialog window
        alertBuilder.setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.okText), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (otpTextView.getText().toString().trim().isEmpty()) {
                            CustomToast.getInstance().showToast(RideDetails.this,getResources().getString(R.string.enterOTPCode),ToastType.INFO,Toast.LENGTH_LONG);
                            return;
                        }
                        DriverProfile driverProfile = SharedPref.getDriverObject(RideDetails.this, SharedPrefKey.fileName);
                        if (driverProfile != null) {
                            if(myLocation != null) {
                                String otpCode = otpTextView.getText().toString();
                                HashMap<String, Object> params = new HashMap<>();
                                params.put("otpCode",otpCode);
                                params.put("rideId", rideInfo.getRideId());
                                params.put("startLatitude", myLocation.latitude);
                                params.put("startLongitude", myLocation.longitude);

                                HashMap<String, String> mHeaders = new HashMap<>();
                                mHeaders.put("authToken", driverProfile.getAuthToken());

                                NetworkRequest networkRequest = new NetworkRequest(RideDetails.this, URLS.startRide, Request.Method.POST, TYPEREQUEST.START_RIDE, params, mHeaders, false, getResources().getString(R.string.startingRideText), null);
                                networkRequest.setListener(RideDetails.this);
                                networkRequest.getResult();
                            } else {
                                CustomToast.getInstance().showToast(RideDetails.this,getResources().getString(R.string.enableGPSAlertText),ToastType.INFO,Toast.LENGTH_LONG);
                            }
                        }

                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancelText), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // create an alert dialog
        AlertDialog alert = alertBuilder.create();
        if(alert.getWindow() != null){
            alert.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
        alert.show();
    }

    public void showPricePromptDialog(){
            //get OTP code view
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.ride_price_view,null);
        final EditText priceTextView = (EditText) promptView.findViewById(R.id.priceView);
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setTitle(getResources().getString(R.string.rideCompletedTitle));
//            alertBuilder.setMessage("Have you successfully reached the destination?");
            alertBuilder.setView(promptView);


            //setup a dialog window
            alertBuilder.setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.okText), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(!validatePriceView(priceTextView.getText().toString())){
                                return;
                            }
                            endRide(priceTextView.getText().toString());
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancelText), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            // create an alert dialog
            AlertDialog alert = alertBuilder.create();
        if(alert.getWindow() != null){
            alert.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
        alert.show();

    }

    public void showCustomerImage(String customerImage) {
            String imageURL = URLS.driverImageURL + customerImage;
            imageLoader.get(imageURL, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    profileImageCustomer.setImageBitmap(response.getBitmap());
                    profileImageCustomer2.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap() != null) {
                        Bitmap icon = ((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap();
                        profileImageCustomer.setImageBitmap(icon);
                        profileImageCustomer2.setImageBitmap(icon);
                    }
                }
            });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    private void saveRideDistance(Float distance){
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DISTANCE,distance);
    }

    public Bitmap createScaledTaxiMarker(){
        BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.taxi_marker);
        Bitmap b = bitmapDrawable.getBitmap();
        final float scale = getResources().getDisplayMetrics().density;
        int p = (int) (Key.CAR_SIZE * scale + 0.5f);
        return Bitmap.createScaledBitmap(b,p,p,false);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(newLocation != null) {
            if (distanceCalculation) {
                distanceMeasured += newLocation.distanceTo(location);
                dist = String.format("%.2f", distanceMeasured / 1000);
                distance.setText(dist + " km");
                //saveRideDistance(distanceMeasured);
            }
        }

        myLocation = new LatLng(location.getLatitude(),location.getLongitude());

        if(myLocationMarker == null){
            myLocationClone = location;
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,15));
            myLocationMarker = map.addMarker(new MarkerOptions() .title(getResources().getString(R.string.myLocationText)) .position(myLocation) .anchor(0.5f,0.5f));
            myLocationMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createScaledTaxiMarker()));
            if(rideInfo != null){
                MapFunction mapFunction = new MapFunction(this,map,myLocation,rideInfo.getCustomerLocation(),rideInfo.getDestinationLocation());
                mapFunction.showPath();
            }

        } else {
            if(newLocation != null){
                rotateMarker(myLocationMarker,newLocation.bearingTo(location));
            }
            LatLng loc = new LatLng(location.getLatitude(),location.getLongitude());
            animateMarker(loc,false);

                    cameraPosition = new CameraPosition.Builder()
                            .target(myLocation)
                            .zoom(15)
                            .bearing(location.getBearing())                // Sets the orientation of the camera
                            .build();                   // Creates a CameraPosition from the builder
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        newLocation = location;

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        if(snackbar != null){
            if(snackbar.isShown()){
                snackbar.dismiss();
            }
        }
        configureLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        snackbar = Snackbar.make(layout, getResources().getString(R.string.gps_disabled), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getResources().getString(R.string.action_settings), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        snackbar.show();
    }

    public void rotateMarker(final Marker marker, final float toRotation) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public void animateMarker(final LatLng toPosition,final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(myLocationMarker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                myLocationMarker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        myLocationMarker.setVisible(false);
                    } else {
                        myLocationMarker.setVisible(true);
                    }
                }
            }
        });
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
       Log.d("Start Ride response:","" +  result);
        if(typeRequest == TYPEREQUEST.START_RIDE){
            try {
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1){
                            SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,true);
                            DriverProfile profile = SharedPref.getDriverObject(RideDetails.this,SharedPrefKey.fileName);
                            if(profile != null){
                                SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,profile.getDriverId());
                            }
                            customerDetailLayout.setVisibility(View.INVISIBLE);
                            ratingLayout.setVisibility(View.INVISIBLE);
                            endRideLayout.setVisibility(View.VISIBLE);
                            //setChronometer();
                            updateTime.startTime();
                            distanceCalculation = true;
                        }
                        else {
                            Log.d("Start Ride:","Failed");
                        }
                    } else if(result.has("error")){
                        String error = result.getString("message");
                        CustomToast.getInstance().showToast(context,error,ToastType.ERROR,Toast.LENGTH_SHORT);
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (typeRequest == TYPEREQUEST.END_RIDE){
            try {
                if (result != null) {
                    if (result.has(Key.success)) {
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1) {
                            //ViewGroup parent = (ViewGroup) customerDetailLayout.getParent();
                            //parent.removeView(customerDetailLayout);
                            JSONObject rangeInfo = result.getJSONObject("extendedDetails");
                            extendedRange = rangeInfo.getBoolean("extendedRange");
                            extendedRangePrice = rangeInfo.getDouble("extendedRangePrice");
                            clearRideDistance();
                            customerDetailLayout.setVisibility(View.INVISIBLE);
                            endRideLayout.setVisibility(View.INVISIBLE);
                            ratingLayout.setVisibility(View.VISIBLE);
                            customerNameRate.setText(rideInfo.getCustomer().getFirstName() + " " + rideInfo.getCustomer().getLastName());
                            //SAve rate Request status to Shared pref
                            SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,true);
                            DriverProfile profile = SharedPref.getDriverObject(RideDetails.this,SharedPrefKey.fileName);
                            if(profile != null){
                                SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,profile.getDriverId());
                            }
                            String meterPrice =  SharedPref.readFromPreference(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_METER_PRICE,"");
                            if(!meterPrice.equals("")){
                                subTotal.setText(meterPrice);
                                Double price = Double.valueOf(meterPrice);
                                Double total = price + serviceCharge + extendedRangePrice;
                                finalTotal.setText("Rs " + String.valueOf(total));
                            }
                            if(extendedRange){
                                extraCharge.setText(extendedRangePrice + "");
                            }
                            stopService(new Intent(this, RideInformationService.class));

                            if(NetworkRequest.progressDialog != null){
                                if(NetworkRequest.progressDialog.isShowing()){
                                    NetworkRequest.progressDialog.dismiss();
                                }
                            }
                        } else {
                           CustomToast.getInstance().showToast(this,getResources().getString(R.string.error),ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (typeRequest == TYPEREQUEST.UPDATE_CUSTOMER_RATING){
            try {
                if (result != null)
                    if (result.has(Key.success)) {
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1) {
                            CustomToast.getInstance().showToast(this,getResources().getString(R.string.rideCompletedText),ToastType.SUCCESS,Toast.LENGTH_SHORT);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,0);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,false);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,"");
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,0);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_METER_PRICE,"");

                            Intent intent = new Intent(RideDetails.this, Home.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            CustomToast.getInstance().showToast(this,getResources().getString(R.string.error),ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (typeRequest == TYPEREQUEST.RIDE_ACTIVITY_STATUS){
                Log.d("RideInfo","" + result);
                try {
                    JSONObject rideData = result.getJSONObject("currentRideDetails");

                    Double newDriverLatitude = rideData.getDouble("driverLatitude");
                    Double newDriverLongitude = rideData.getDouble("driverLongitude");
                    Boolean startRideStatus = rideData.getBoolean("startRideStatus");
                    Boolean successStatus = rideData.getBoolean("successStatus");
                    Boolean cancelledStatus = rideData.getBoolean("cancelledStatus");
                    Boolean activeRideStatus = rideData.getBoolean("activeRideStatus");
                    Boolean expireStatus = rideData.getBoolean("expireStatus");


                    RideActivity rideActivity = RideActivity.getInstance();
                    rideActivity.setDriverLocation(new LatLng(newDriverLatitude,newDriverLongitude));
                    rideActivity.setRideStarted(startRideStatus);
                    rideActivity.setRideSuccess(successStatus);
                    rideActivity.setRideCancelled(cancelledStatus);
                    rideActivity.setRideActive(activeRideStatus);
                    rideActivity.setExpireStatus(expireStatus);

                    updateUIFromRideActivity(rideActivity);

                } catch (JSONException e){
                    e.printStackTrace();
            }
        }
    }

    public void updateUIFromRideActivity(RideActivity rideActivity){
        if(rideActivity.getExpireStatus() || rideActivity.getRideCancelled()){
            if(rideActivitiesTimer != null){
                rideActivitiesTimer.cancel();
                rideActivitiesTimer.purge();
            }
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,0);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,"");
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,0);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_METER_PRICE,"");

            CustomToast.getInstance().showToast(this,getResources().getString(R.string.rideExpiredText),ToastType.ERROR,Toast.LENGTH_SHORT);

            Intent i = new Intent(RideDetails.this, Home.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

        if(rideActivity.getRideSuccess()){
            if(rideActivitiesTimer != null){
                rideActivitiesTimer.cancel();
                rideActivitiesTimer.purge();
            }
            //Save rate Request status to Shared pref
            SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,true);
            DriverProfile profile = SharedPref.getDriverObject(RideDetails.this,SharedPrefKey.fileName);
            if(profile != null){
                SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,profile.getDriverId());
            }
            checkPreviousState();
        }
        else if(rideActivity.getRideStarted()){
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,true);
            DriverProfile profile = SharedPref.getDriverObject(RideDetails.this,SharedPrefKey.fileName);
            if(profile != null){
                SharedPref.saveToPreferences(RideDetails.this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,profile.getDriverId());
            }
            endRideLayout.setVisibility(View.VISIBLE);
            distanceCalculation = true;
        }
    }


    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR,Toast.LENGTH_SHORT);
    }

    private void setAllLayouts(){
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        imageLoader = volleySingleton.getImageLoader();
        layout = (CoordinatorLayout) findViewById(R.id.detail_layout);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        customerDetailLayout = (LinearLayout) findViewById(R.id.customer_details);
        customerDetailLayout.setVisibility(View.INVISIBLE);
        endRideLayout = (LinearLayout) findViewById(R.id.ride_end_layout);
        ratingLayout = (LinearLayout) findViewById(R.id.rating_layout);
        customerRating = (TextView) findViewById(R.id.customer_rating);
        timer = (TextView) findViewById(R.id.timer);
        distance = (TextView) findViewById(R.id.distance);
        profileImageCustomer = (CircleImageView) findViewById(R.id.profile_image);
        profileImageCustomer2 = (CircleImageView) findViewById(R.id.profile_image_customer);
        chronometer = (Chronometer) findViewById(R.id.chronometer);

        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.ride_map);
        name = (TextView) findViewById(R.id.name);
        phone = (TextView) findViewById(R.id.phone);
        callCustomerBtn = (ImageButton) findViewById(R.id.call_image);
        startRideBtn = (Button) findViewById(R.id.start_ride_button);
        endRideBtn = (Button) findViewById(R.id.end_ride_button);
        customerNameRate = (TextView) findViewById(R.id.customer_name_rate);
        rateButton = (Button) findViewById(R.id.rate_button);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        subTotal = (TextView) findViewById(R.id.subTotal);
        finalTotal = (TextView) findViewById(R.id.finalTotal);

        cardView = (CardView) findViewById(R.id.card_view);
        extendedRangeText = (TextView) findViewById(R.id.extended_range_text);
        extraCharge = (TextView) findViewById(R.id.extraCharge);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearRideDistance();
    }

    private void clearRideDistance(){
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DISTANCE,0);
    }

    private boolean validatePriceView(String price){
        if (price.trim().isEmpty()) {
            CustomToast.getInstance().showToast(RideDetails.this,getResources().getString(R.string.enterPriceText),ToastType.INFO,Toast.LENGTH_LONG);
            return false;
        }
        if(price.charAt(0) == '.'){
            CustomToast.getInstance().showToast(RideDetails.this,getResources().getString(R.string.entervalidPriceText),ToastType.INFO,Toast.LENGTH_LONG);
            return false;
        }
        if(price.length() > 8){
            CustomToast.getInstance().showToast(RideDetails.this,getResources().getString(R.string.invalidPrice),ToastType.INFO,Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }

    public BroadcastReceiver rideInformationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Gson gson = new Gson();
            String rideDetailsString = intent.getExtras().getString("rideDetails");
            RideActivity ride = gson.fromJson(rideDetailsString,RideActivity.class);
            if(ride.getExpireStatus()){
                showDialog(getResources().getString(R.string.rideExpiredTitle),getResources().getString(R.string.rideExpiredText));
            }else if (ride.getRideCancelled()){
                Toast.makeText(context, getResources().getString(R.string.rideCancelledTitle), Toast.LENGTH_SHORT).show();
                showDialog(getResources().getString(R.string.rideCancelledTitle),getResources().getString(R.string.rideCancelledText));
            }else if(ride.getRideSuccess()){
                checkPreviousState();
            } else if(ride.getRideStarted()){
                endRideLayout.setVisibility(View.VISIBLE);
                distanceCalculation = true;
            }
        }
    };

    public void showDialog(String title,String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.okText), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent i = new Intent(RideDetails.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
