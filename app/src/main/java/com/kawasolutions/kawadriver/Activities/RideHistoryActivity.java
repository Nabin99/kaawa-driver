package com.kawasolutions.kawadriver.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.CustomToast;
import com.kawasolutions.kawadriver.DividerItemDecoration;
import com.kawasolutions.kawadriver.Enum.ToastType;
import com.kawasolutions.kawadriver.Model.RideHistory;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.Adapters.RideHistoryAdapter;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RideHistoryActivity extends AppCompatActivity implements NetworkRequest.NetworkListener{

    private ArrayList<RideHistory> rideHistoryList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private LinearLayout noHistoryLayout;
    private RideHistoryAdapter rideHistoryAdapter;
    private TextView noRide;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    @Nullable
    private String startLocation;
    @Nullable
    private String endLocation;
    @Nullable
    private String imageName;
    @Nullable
    private Double price;
    private Double extendedPrice = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        noHistoryLayout = (LinearLayout) findViewById(R.id.no_data_layout);
        noRide = (TextView) findViewById(R.id.no_ride_history);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        rideHistoryAdapter = new RideHistoryAdapter(rideHistoryList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(rideHistoryAdapter);
        getRideHistory();

    }

    public void getRideHistory(){
        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if (driverProfile != null) {
            HashMap<String,String> mHeaders = new HashMap<>();
            mHeaders.put("authToken",driverProfile.getAuthToken());

            String url = URLS.rideHistory + driverProfile.getDriverId();

            NetworkRequest networkRequest = new NetworkRequest(this, url, Request.Method.GET, TYPEREQUEST.RIDE_HISTORY, null, mHeaders,false, getResources().getString(R.string.gettingRideHistoryText),null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }
    }

    public void parseJSONResponse(JSONObject response){
        try {
            JSONArray details = response.getJSONArray("rideHistory");

            if(details.length() > 0) {
                if(noHistoryLayout.getVisibility() == View.VISIBLE){
                    noHistoryLayout.setVisibility(View.INVISIBLE);
                }
                for (int i = 0; i < details.length(); i++) {

                    RideHistory rideHistory = new RideHistory();
                    JSONObject rideHistoryDetail = details.getJSONObject(i);
                    String customerId = rideHistoryDetail.getString("customerId");
                    String rideId = rideHistoryDetail.getString("rideId");
                    String customerName = rideHistoryDetail.getString("customerFirstName") + " " + rideHistoryDetail.getString("customerLastName");
                    String date = rideHistoryDetail.getString("rideDate");
                    String status = rideHistoryDetail.getString("successStatus");
                    String cancelStatus = rideHistoryDetail.getString("cancelStatus");

                    if(!rideHistoryDetail.isNull("startLocation")){
                        startLocation = rideHistoryDetail.getString("startLocation");
                    }
                    if(!rideHistoryDetail.isNull("endLocation")){
                        endLocation = rideHistoryDetail.getString("endLocation");
                    }
                    if(!rideHistoryDetail.isNull("image")){
                        imageName = rideHistoryDetail.getString("image");
                    }
                    if(!rideHistoryDetail.isNull("meterPrice")){
                        price = rideHistoryDetail.getDouble("meterPrice");
                    }
                    if(!rideHistoryDetail.isNull("extendedPrice")){
                        extendedPrice = rideHistoryDetail.getDouble("extendedPrice");
                    }

                    rideHistory.setCustomerId(customerId);
                    rideHistory.setRideId(rideId);
                    rideHistory.setCustomerName(customerName);
                    rideHistory.setDate(date);
                    rideHistory.setStatus(status);
                    rideHistory.setCancelStatus(cancelStatus);
                    rideHistory.setStartLocation(startLocation);
                    rideHistory.setEndLocation(endLocation);
                    rideHistory.setImageName(imageName);
                    rideHistory.setPrice(price);
                    rideHistory.setExtendedPrice(extendedPrice);

                    rideHistoryList.add(rideHistory);

                    rideHistoryAdapter.notifyDataSetChanged();
                }
            } else {
                if(noHistoryLayout.getVisibility() == View.INVISIBLE){
                    noHistoryLayout.setVisibility(View.VISIBLE);
                    noRide.setText(getResources().getString(R.string.no_trips));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, Home.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.RIDE_HISTORY){
            try {
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1){
                            parseJSONResponse(result);
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR,Toast.LENGTH_SHORT);
    }
}
