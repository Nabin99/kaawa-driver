package com.kawasolutions.kawadriver.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.CustomToast;
import com.kawasolutions.kawadriver.Enum.ToastType;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.NetworkConnection;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Utils.VolleyErrorMessage;

import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements NetworkRequest.NetworkListener{


    private TextInputLayout inputPhone;
    private TextInputLayout inputPassword;
    private EditText phone;
    private EditText password;

    private Button LoginButton;
    private Context context;

    private ProgressDialog progressDialog;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;
        inputPhone = (TextInputLayout) findViewById(R.id.input_phone);
        inputPassword = (TextInputLayout) findViewById(R.id.input_password);

        phone = (EditText) findViewById(R.id.phone);
        password = (EditText) findViewById(R.id.password);
        LoginButton = (Button) findViewById(R.id.login_form_button);

        phone.addTextChangedListener(new MyTextWatcher(phone));
        password.addTextChangedListener(new MyTextWatcher(password));
        final NetworkConnection networkConnection = new NetworkConnection(this);

        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (networkConnection.isConnected()) {
                    submitForm();
                } else {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                }

            }
        });

        inputPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFocus(phone);
            }
        });

        inputPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFocus(password);
            }
        });

    }

    public void submitForm() {
        if (!validatePhone()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }

        String phone_number = phone.getText().toString();
        String pass = password.getText().toString();

        HashMap<String, Object> params = new HashMap<>();
        params.put("contactNo", phone_number);
        params.put("password", pass);

        NetworkRequest networkRequest = new NetworkRequest(this, URLS.loginPost, Request.Method.POST, TYPEREQUEST.LOGIN, params, null, false, getResources().getString(R.string.pleaseWaitText), null);
        networkRequest.setListener(this);
        networkRequest.getResult();

    }

    public boolean validatePhone(){
        if (phone.getText().toString().trim().isEmpty()) {
            inputPhone.setError(getString(R.string.error_field_required));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;
        } else if(phone.getText().length() < 10){
            inputPhone.setError(getString(R.string.error_phone_length));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;

        }else{
            inputPhone.setErrorEnabled(false);
        }
        return true;
    }

    public boolean validatePassword(){
        if (password.getText().toString().trim().isEmpty()) {
            inputPassword.setError(getString(R.string.error_field_required));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        } else if (password.getText().length() < 6){
            inputPassword.setError(getString(R.string.error_password_length));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        }
        else{
            inputPassword.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.LOGIN){
            try{

                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);

                        if (success == 1){
                            JSONObject driverData = result.getJSONObject("driverDetails");

                            Integer driverId = driverData.getInt("driverId");
                            String firstname = driverData.getString("firstName");
                            String lastname = driverData.getString("lastName");
                            String email = driverData.optString("email");
                            String gender = driverData.getString("gender");
                            Double rating = driverData.optDouble("averageRating");



                            JSONObject userData = driverData.getJSONObject("userId");
                            Integer userId = userData.getInt("userId");

                            String phone = userData.getString("contactNo");
                            String username = userData.optString("username");
                            String authToken = userData.getString("authToken");

                            JSONObject vehicleData = driverData.getJSONObject("vehicleId");
                            String vehicletype = vehicleData.getString("vehicleType");
                            String vehicleno = vehicleData.getString("vehicleNo");

                            DriverProfile driverProfile = new DriverProfile();
                            driverProfile.setDriverId(driverId);
                            driverProfile.setFirstName(firstname);
                            driverProfile.setLastName(lastname);
                            driverProfile.setEmail(email);
                            driverProfile.setGender(gender);
                            driverProfile.setAverageRating(rating);
                            driverProfile.setUserId(userId);
                            driverProfile.setContactNo(phone);
                            driverProfile.setUserName(username);
                            driverProfile.setAuthToken(authToken);
                            driverProfile.setVehicleType(vehicletype);
                            driverProfile.setVehicleNo(vehicleno);
                            driverProfile.setLoggedIn(true);

                            Gson gson = new Gson();
                            String driverProfileString = gson.toJson(driverProfile);
                            SharedPref.saveToPreferences(context, SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_PROFILE,driverProfileString);
                            SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_ONLINE,true);

                            Intent i = new Intent(LoginActivity.this, Home.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();

                        }else{
                            String error = result.getString(Key.error);
                            CustomToast.getInstance().showToast(this,error, ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }

            }catch (Exception e){
                Log.d("ExceptionDriver:" ,"" + e.toString());
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error),ToastType.ERROR,Toast.LENGTH_SHORT);
    }


    private class MyTextWatcher implements TextWatcher {

        View view;

        public MyTextWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){

                case R.id.phone:
                    validatePhone();
                    break;

                case R.id.password:
                    validatePassword();
                    break;
            }

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
