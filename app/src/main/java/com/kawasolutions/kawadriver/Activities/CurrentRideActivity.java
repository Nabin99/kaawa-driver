package com.kawasolutions.kawadriver.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class CurrentRideActivity extends AppCompatActivity {

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private TextView customer_name;
    private TextView customer_phone;
    private TextView pickup_place;
    private ProgressDialog progressDialog;

    private Double start_latitude;
    private Double start_longitude;
    private String startAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_ride);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Current Ride");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        customer_name = (TextView) findViewById(R.id.customerName);
        customer_phone = (TextView) findViewById(R.id.customerPhone);
        pickup_place = (TextView) findViewById(R.id.PickUpPlaceName);
        getCurrentRideInfo();


    }

    public void getCurrentRideInfo() {
        String driverId = SharedPref.readFromPreference(this, SharedPrefKey.fileName, "pref_driverID", "");


        String url = URLS.driverCurrentRide + driverId;
        Log.d("URL current ride:", "" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("Response current ride:", "" + response);
                try {
                    if (response != null) {

                        Integer success = 0;

                        if (response.has(Key.success)) {

                            success = (Integer) response.get(Key.success);

                            if (success == 1) {
                                parseJSONResponse(response);

                            } else {
                                Toast.makeText(CurrentRideActivity.this, "Location not found", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CurrentRideActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        requestQueue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

    }

    public void parseJSONResponse(JSONObject response) {
        try {

            JSONObject jsonObject = response.getJSONObject("lastTrip");

            String customerFirstName = Character.toUpperCase(jsonObject.getString("customerFirstName").charAt(0)) + jsonObject.getString("customerFirstName").substring(1);
            String customerLastName = Character.toUpperCase(jsonObject.getString("customerLastName").charAt(0)) + jsonObject.getString("customerLastName").substring(1);
            String customerName = customerFirstName + " " + customerLastName;
            String customerPhone = jsonObject.getString("customerContactNo");
//            String pickupPlace = jsonObject.getString("PickupPlace");
            start_latitude = jsonObject.getDouble("startLatitude");
            start_longitude = jsonObject.getDouble("startLongitude");
            locationStartAddress("start");
            customer_name.setText(customerName);
            customer_phone.setText(customerPhone);
            pickup_place.setText(startAddress);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void locationStartAddress(String type) {
//
        Geocoder geocoder = new Geocoder(CurrentRideActivity.this);
        List<Address> addresses;

        try {
            if (type == "start") {
                addresses = geocoder.getFromLocation(start_latitude, start_longitude, 1);
                if (addresses.size() == 0) {
                    Toast.makeText(CurrentRideActivity.this, "Location not Found", Toast.LENGTH_SHORT).show();
                } else {
                    String address = addresses.get(0).getAddressLine(0);
                    String locality = addresses.get(0).getLocality();
                    startAddress = locality + ", " + address;

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, Home.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

}

