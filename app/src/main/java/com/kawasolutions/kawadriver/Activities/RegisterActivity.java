package com.kawasolutions.kawadriver.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.NetworkConnection;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {


    private TextInputLayout inputFirstName;
    private TextInputLayout inputLastName;
    private TextInputLayout inputLicenseNo;
    private TextInputLayout inputCarModel;
    private TextInputLayout inputCarNumber;
    private TextInputLayout inputPhone;
    //private TextInputLayout inputPhone2;
    private TextInputLayout inputPassword;
    private TextInputLayout inputconfirmPassword;
    private EditText firstName;
    private EditText lastName;
    private EditText licenseno;
    private EditText carmodel;
    private EditText carnumber;
    private EditText phone;
    //private EditText phone2;
    private EditText password;
    private EditText confirmPassword;
    private Button submitBtn;
    private RadioGroup radiosexGroup;
    private RadioButton radiosexButton;
    private Spinner carModel;
    private String car_model;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    private ProgressDialog progressDialog;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;
        inputFirstName = (TextInputLayout) findViewById(R.id.input_firstname);
        inputLastName = (TextInputLayout) findViewById(R.id.input_lastname);
        inputLicenseNo = (TextInputLayout) findViewById(R.id.input_licensenumber);
        inputCarModel= (TextInputLayout) findViewById(R.id.input_carmodel);
        inputCarNumber = (TextInputLayout) findViewById(R.id.input_carnumber);
        inputPhone = (TextInputLayout) findViewById(R.id.input_phone);
        //inputPhone2 = (TextInputLayout) findViewById(R.id.input_phone2);
        inputPassword = (TextInputLayout) findViewById(R.id.input_password);
        inputconfirmPassword = (TextInputLayout) findViewById(R.id.input_confirm_password);

        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        licenseno = (EditText) findViewById(R.id.licensenumber);
        carModel = (Spinner) findViewById(R.id.car_model);
        carnumber = (EditText) findViewById(R.id.carnumber);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmpassword);
        phone = (EditText) findViewById(R.id.phone);
        //phone2 = (EditText) findViewById(R.id.phone2);
        radiosexGroup = (RadioGroup) findViewById(R.id.gender_group);
        submitBtn = (Button) findViewById(R.id.email_register_button);

        firstName.addTextChangedListener(new MyTextWatcher(firstName));
        lastName.addTextChangedListener(new MyTextWatcher(lastName));
        licenseno.addTextChangedListener(new MyTextWatcher(licenseno));
        //carmodel.addTextChangedListener(new MyTextWatcher(carmodel));
        carnumber.addTextChangedListener(new MyTextWatcher(carnumber));
        password.addTextChangedListener(new MyTextWatcher(password));
        confirmPassword.addTextChangedListener(new MyTextWatcher(confirmPassword));
        phone.addTextChangedListener(new MyTextWatcher(phone));


        final NetworkConnection networkConnection = new NetworkConnection(this);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.car_model,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        carModel.setAdapter(adapter);

        carModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                car_model = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                car_model = parent.getItemAtPosition(0).toString();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Click", "" + "Clicked");
                if (networkConnection.isConnected()) {
                    submitForm();
                } else {
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                }

            }
        });

        inputFirstName.setOnClickListener(this);
        inputLastName.setOnClickListener(this);
        inputLicenseNo.setOnClickListener(this);
        inputCarNumber.setOnClickListener(this);
        inputPhone.setOnClickListener(this);
        //inputPhone2.setOnClickListener(this);
        inputPassword.setOnClickListener(this);
        inputconfirmPassword.setOnClickListener(this);



    }

    public void submitForm(){

        if (!validateFirstName()){
            return;
        }
        if (!validateLastName()){
            return;
        }
        if (!validateLicenseNo()){
            return;
        }
        if (!validateCarNumber()){
            return;
        }

        if (!validatePassword()){
            return;
        }
        if (!validateconfirmPassword()){
            return;
        }

        if (!validatePhone()){
            return;
        }

        String fname = firstName.getText().toString();
        String lname = lastName.getText().toString();
        String license = licenseno.getText().toString();
        String carNumber = carnumber.getText().toString();
        String number = phone.getText().toString();
       // String number2 = phone2.getText().toString();
        String pass = password.getText().toString();



        int selectedId = radiosexGroup.getCheckedRadioButtonId();
        radiosexButton = (RadioButton) findViewById(selectedId);
        String sex = radiosexButton.getText().toString();


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("firstName", fname);
        params.put("lastName", lname);
        params.put("licenceNo", license);
        params.put("vehicleType", car_model);
        params.put("vehicleNo", carNumber);
        params.put("password", pass);
        params.put("contactNo", number);
       // params.put("contanctNo2", number2);
        params.put("gender", sex);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.registerPost,
                new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("Response:", "" + response);
                try {
                    if (response != null){

                        Integer success = 0;

                        if (response.has(Key.success)){

                            success = (Integer) response.get(Key.success);

                            if (success == 1){
                                Toast.makeText(RegisterActivity.this, getResources().getString(R.string.accountRegistrationSuccessfulText), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(RegisterActivity.this,MainActivity.class));
                                finish();
                            }
                            else {
                                String error = response.getString(Key.error);
                                Toast.makeText(RegisterActivity.this,error, Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterActivity.this ,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        requestQueue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(getResources().getString(R.string.registeringAccountText));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

    }


    private boolean validateFirstName(){
        if (firstName.getText().toString().trim().isEmpty()) {
            inputFirstName.setError(getString(R.string.error_field_required));
            firstName.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(firstName);
            return false;
        } else {
            inputFirstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName(){
        if (lastName.getText().toString().trim().isEmpty()) {
            inputLastName.setError(getString(R.string.error_field_required));
            lastName.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(lastName);
            return false;
        } else {
            inputLastName.setErrorEnabled(false);
        }

        return true;

    }



    private boolean validateLicenseNo(){
        String LicenseStr = licenseno.getText().toString().trim();

        if (LicenseStr.isEmpty()) {
            inputLicenseNo.setError(getString(R.string.error_field_required));
            licenseno.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(licenseno);
            return false;
        }
        else {
            inputLicenseNo.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCarModel(){
        if (carmodel.getText().toString().trim().isEmpty()) {
            inputCarModel.setError(getString(R.string.error_field_required));
            carmodel.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(carmodel);
            return false;
        } else {
            inputCarModel.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validateCarNumber(){
        if (carnumber.getText().toString().trim().isEmpty()) {
            inputCarNumber.setError(getString(R.string.error_field_required));
            carnumber.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(carnumber);
            return false;
        } else {
            inputCarNumber.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validatePassword(){
        if (password.getText().toString().trim().isEmpty()) {
            inputPassword.setError(getString(R.string.error_field_required));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        } else if (password.getText().length() < 6){
            inputPassword.setError(getString(R.string.error_password_length));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        }
        else{
            inputPassword.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validateconfirmPassword(){
        if (confirmPassword.getText().toString().trim().isEmpty()) {
            inputconfirmPassword.setError(getString(R.string.error_field_required));
            confirmPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(confirmPassword);
            return false;
        } else if (!confirmPassword.getText().toString().equals(password.getText().toString())){
            inputconfirmPassword.setError(getString(R.string.error_password_match));
            confirmPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(confirmPassword);
            return false;
        }
        else{
            inputconfirmPassword.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validatePhone(){
        if (phone.getText().toString().trim().isEmpty()) {
            inputPhone.setError(getString(R.string.error_field_required));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;
        } else if(phone.getText().length() < 10){
            inputPhone.setError(getString(R.string.error_phone_length));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;

        }else{
            inputPhone.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.input_firstname:
                requestFocus(firstName);
                break;

            case R.id.input_lastname:
                requestFocus(lastName);
                break;

            case R.id.input_licensenumber:
                requestFocus(licenseno);
                break;

            case R.id.input_carnumber:
                requestFocus(carnumber);
                break;

            case R.id.input_password:
                requestFocus(password);
                break;

            case R.id.input_confirm_password:
                requestFocus(confirmPassword);
                break;

            case R.id.input_phone:
                requestFocus(phone);
                break;

//            case R.id.input_phone2:
//                requestFocus(phone2);
//                break;

            default:
                break;
        }
    }


    private class MyTextWatcher implements TextWatcher {

        View view;

        public MyTextWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.first_name:
                    validateFirstName();
                    break;
                case R.id.last_name:
                    validateLastName();
                    break;
                case R.id.licensenumber:
                    validateLicenseNo();
                    break;
                case R.id.car_model:
                    validateCarModel();
                    break;
                case R.id.carnumber:
                    validateCarNumber();
                    break;

                case R.id.password:
                    validatePassword();
                    break;
                case R.id.confirmpassword:
                    validateconfirmPassword();
                    break;
                case R.id.phone:
                    validatePhone();
                    break;

                default:
                    break;
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
