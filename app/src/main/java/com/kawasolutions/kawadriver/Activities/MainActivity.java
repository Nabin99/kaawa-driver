package com.kawasolutions.kawadriver.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.CustomToast;
import com.kawasolutions.kawadriver.Enum.ToastType;
import com.kawasolutions.kawadriver.BuildConfig;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.NetworkConnection;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NetworkRequest.NetworkListener {

    private TextInputLayout inputPhone;
    private TextInputLayout inputPassword;
    private EditText phone;
    private EditText password;
    private TextView termsText;
    private Button LoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //checkForUpdates();
        Boolean newUpdate = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_NEW_APP_UPDATE, false);
        Boolean showUpdatePage = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.SHOW_UPDATE_PAGE, false);
        if (newUpdate && showUpdatePage) {
            startActivity(new Intent(MainActivity.this, NewUpdateActivity.class));
        } else {
            DriverProfile profile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
            if (profile != null && profile.getLoggedIn()) {
                startActivity(new Intent(MainActivity.this, Home.class));
                finish();
            } else {
                setContentView(R.layout.activity_main);

                inputPhone = (TextInputLayout) findViewById(R.id.input_phone);
                inputPassword = (TextInputLayout) findViewById(R.id.input_password);

                phone = (EditText) findViewById(R.id.phone);
                password = (EditText) findViewById(R.id.password);
                termsText = (TextView) findViewById(R.id.btn_terms);
                LoginButton = (Button) findViewById(R.id.login_form_button);

                phone.addTextChangedListener(new MainActivity.MyTextWatcher(phone));
                password.addTextChangedListener(new MainActivity.MyTextWatcher(password));
                final NetworkConnection networkConnection = new NetworkConnection(this);

                termsText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTermsAndCondition();
                    }
                });

                LoginButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (networkConnection.isConnected()) {
                            submitForm();
                        } else {
                            CustomToast.getInstance().showToast(MainActivity.this, getResources().getString(R.string.error_network), ToastType.ERROR, Toast.LENGTH_SHORT);
                        }

                    }
                });

                inputPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestFocus(phone);
                    }
                });

                inputPassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestFocus(password);
                    }
                });
            }
        }
    }

    public void submitForm() {
        if (!validatePhone()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }

        String phone_number = phone.getText().toString();
        String pass = password.getText().toString();

        HashMap<String, Object> params = new HashMap<>();
        params.put("contactNo", phone_number);
        params.put("password", pass);

        NetworkRequest networkRequest = new NetworkRequest(this, URLS.loginPost, Request.Method.POST, TYPEREQUEST.LOGIN, params, null, false, getResources().getString(R.string.pleaseWaitText), null);
        networkRequest.setListener(this);
        networkRequest.getResult();

    }

    public boolean validatePhone(){
        if (phone.getText().toString().trim().isEmpty()) {
            inputPhone.setError(getString(R.string.error_field_required));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;
        } else if(phone.getText().length() < 10){
            inputPhone.setError(getString(R.string.error_phone_length));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;

        }else{
            inputPhone.setErrorEnabled(false);
        }
        return true;
    }

    public boolean validatePassword(){
        if (password.getText().toString().trim().isEmpty()) {
            inputPassword.setError(getString(R.string.error_field_required));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        } else if (password.getText().length() < 6){
            inputPassword.setError(getString(R.string.error_password_length));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        }
        else{
            inputPassword.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.LOGIN){
            try{

                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);

                        if (success == 1){
                            JSONObject driverData = result.getJSONObject("driverDetails");

                            Integer driverId = driverData.getInt("driverId");
                            String firstname = driverData.getString("firstName");
                            String lastname = driverData.getString("lastName");
                            String email = driverData.optString("email");
                            String gender = driverData.getString("gender");
                            Double rating = driverData.optDouble("averageRating");



                            JSONObject userData = driverData.getJSONObject("userId");
                            Integer userId = userData.getInt("userId");

                            String phone = userData.getString("contactNo");
                            String username = userData.optString("username");
                            String authToken = userData.getString("authToken");

                            JSONObject vehicleData = driverData.getJSONObject("vehicleId");
                            String vehicletype = vehicleData.getString("vehicleType");
                            String vehicleno = vehicleData.getString("vehicleNo");

                            DriverProfile driverProfile = new DriverProfile();
                            driverProfile.setDriverId(driverId);
                            driverProfile.setFirstName(firstname);
                            driverProfile.setLastName(lastname);
                            driverProfile.setEmail(email);
                            driverProfile.setGender(gender);
                            driverProfile.setAverageRating(rating);
                            driverProfile.setUserId(userId);
                            driverProfile.setContactNo(phone);
                            driverProfile.setUserName(username);
                            driverProfile.setAuthToken(authToken);
                            driverProfile.setVehicleType(vehicletype);
                            driverProfile.setVehicleNo(vehicleno);
                            driverProfile.setLoggedIn(true);

                            Gson gson = new Gson();
                            String driverProfileString = gson.toJson(driverProfile);
                            SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_PROFILE,driverProfileString);
                            SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_ONLINE,true);

                            Intent i = new Intent(MainActivity.this, Home.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();

                        }else{
                            String error = result.getString(Key.error);
                            CustomToast.getInstance().showToast(this,error, ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }

            }catch (Exception e){
                Log.d("ExceptionDriver:" ,"" + e.toString());
            }
        }else if(typeRequest == TYPEREQUEST.CHECK_APP_VERSION){
            try {
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get("success");
                        if (success == 1){
                            String latestAppVersion = result.getString("version");
                            Integer latest = Integer.parseInt(latestAppVersion);
                            Integer currentAppVersion = BuildConfig.VERSION_CODE;
                            if(latest > currentAppVersion){
                                Integer version = SharedPref.readIntegerFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_NEW_APP_VERSION);
                                if(!version.equals(latest)) {
                                    SharedPref.saveToPreferences(this, SharedPrefKey.fileName, SharedPrefKey.KEY_NEW_APP_UPDATE, true);
                                    SharedPref.saveToPreferences(this, SharedPrefKey.fileName, SharedPrefKey.KEY_NEW_APP_VERSION, latest);
                                    SharedPref.saveToPreferences(this, SharedPrefKey.fileName, SharedPrefKey.SHOW_UPDATE_PAGE, true);
                                }
                            } else {
                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_NEW_APP_UPDATE,false);
                            }
                        } else {
                            Log.d("Get App Update Error:","Error while checking for app updates");
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error),ToastType.ERROR,Toast.LENGTH_SHORT);
    }


    private class MyTextWatcher implements TextWatcher {

        View view;

        public MyTextWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){

                case R.id.phone:
                    validatePhone();
                    break;

                case R.id.password:
                    validatePassword();
                    break;
            }

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void showTermsAndCondition(){
        String url = getResources().getString(R.string.termsAndConditionURL);
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
        startActivity(intent);
    }

    public void checkForUpdates(){
        NetworkRequest networkRequest = new NetworkRequest(this, URLS.checkAppVersion, Request.Method.GET, TYPEREQUEST.CHECK_APP_VERSION,true, "",null);
        networkRequest.setListener(this);
        networkRequest.getResult();
    }

    public void changeAppLanguage(){
        Locale locale = new Locale("en");
        Locale.setDefault(locale);
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        configuration.locale= locale;
        resources.updateConfiguration(configuration,displayMetrics);
    }

}
