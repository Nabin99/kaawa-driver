package com.kawasolutions.kawadriver.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.CustomToast;
import com.kawasolutions.kawadriver.Enum.ToastType;
import com.kawasolutions.kawadriver.LoginStatus;
import com.kawasolutions.kawadriver.Model.RideInfo;
import com.kawasolutions.kawadriver.Service.MediaPlayerAndVibratorService;
import com.kawasolutions.kawadriver.Singleton.VolleySingleton;
import com.kawasolutions.kawadriver.BuildConfig;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.GCM.MyGcmListenerService;
import com.kawasolutions.kawadriver.GCM.RegistrationIntentService;
import com.kawasolutions.kawadriver.Model.CallNotification;
import com.kawasolutions.kawadriver.Model.Customer;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.R;
import com.kawasolutions.kawadriver.Service.MyLocationService;
import com.kawasolutions.kawadriver.Utils.PopUp;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Home extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, NetworkRequest.NetworkListener,Switch.OnCheckedChangeListener{

    private static final String TAG = "app status detail";
    private SupportMapFragment mapFragment;
    private GoogleMap mapView;
    protected LocationManager locationManager;
    private LocationListener locationListener;
    private Marker marker;
    LatLng loc;
    private Snackbar snackbar;
    private FrameLayout fullLayout;
    LoginStatus loginStatus;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private LinearLayout callConfirmLayout;
    private ImageButton callAcceptBtn;
    private ImageButton callDeclineBtn;
    private Context context;
    private LinearLayout parent;

    private String customerId;
    private CoordinatorLayout layout;
    public static boolean active = false;

    private String driverId;

    private Integer checkStatusCustomerId, checkStatusRideId;
    private Double checkStatusDestinationLatitude,checkStatusDestinationLongitude;
    private ProgressDialog progressDialog;
    private SwitchCompat onlineSwitch;
    private TextView onlineStatusText;
    private TextView onlineHrText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!checkForPlayServices()){
            return;
        }

        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        toolbar.setTitle(R.string.title_activity_home);
        setSupportActionBar(toolbar);
        context = this;
        fullLayout = (FrameLayout) findViewById(R.id.layout_frame);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        callAcceptBtn = (ImageButton) findViewById(R.id.accept_btn);
        callDeclineBtn = (ImageButton) findViewById(R.id.decline_btn);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        parent = (LinearLayout) findViewById(R.id.call_layout);
        layout = (CoordinatorLayout) findViewById(R.id.home_layout);
        onlineHrText = (TextView) findViewById(R.id.online_hr_text);
        onlineSwitch = (SwitchCompat) findViewById(R.id.status_switch);
        onlineStatusText = (TextView) findViewById(R.id.status_text);
        onlineSwitch.setOnCheckedChangeListener(this);

        driverId = SharedPref.readFromPreference(this, SharedPrefKey.fileName, "pref_driverID", "");

        callConfirmLayout = (LinearLayout) findViewById(R.id.call_confirm_layout);
        loginStatus = new LoginStatus(this);
        mapFragment.getMapAsync(this);

        Bundle bundle = getIntent().getExtras();
        startService(new Intent(Home.this, RegistrationIntentService.class));
        // add current version of driver to server
        addAppVersion();
        if (bundle != null) {
            customerId = bundle.getString("customerId");
            if (bundle.getString("REQUEST") != null) {
                Boolean range = bundle.getBoolean("extendedRange");
                startService(new Intent(this, MediaPlayerAndVibratorService.class));
                addCallLayout(MyGcmListenerService.notificationRequestCount,range);
                MyGcmListenerService.notificationRequestCount++;
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(parent.getChildCount() != 0){
            menu.setGroupVisible(0,false);
            if(onlineSwitch != null) {
                onlineSwitch.setEnabled(false);
            }
        } else {
            menu.setGroupVisible(0,true);
            if(onlineSwitch != null) {
                onlineSwitch.setEnabled(true);
            }
        }
        return true;
    }

    public Bitmap createScaledTaxiMarker(){
        BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.taxi_marker);
        Bitmap b = bitmapDrawable.getBitmap();
        final float scale = getResources().getDisplayMetrics().density;
        int p = (int) (Key.CAR_SIZE * scale + 0.5f);
        return Bitmap.createScaledBitmap(b,p,p,false);
    }

    // add layout with decline and accept button if new notification arrives
    public void addCallLayout(Integer i,Boolean extendedRange) {

        View view = getLayoutInflater().inflate(R.layout.call_confirm_layout, null);
        view.setId(i);    // set view id for each notification


        TextView textView = (TextView) view.findViewById(R.id.notification_number);

        callAcceptBtn = (ImageButton) view.findViewById(R.id.accept_btn);
        callDeclineBtn = (ImageButton) view.findViewById(R.id.decline_btn);
        callAcceptBtn.setTag(i);   // set tag for both button (same as id of view)
        callDeclineBtn.setTag(i);

        callAcceptBtn.setOnClickListener(this);
        callDeclineBtn.setOnClickListener(this);
        if(extendedRange){
            textView.setText(String.valueOf(i + 1));
            //create shake animation from res xml
            Animation shake = AnimationUtils.loadAnimation(this,R.anim.shake);
            shake.setRepeatCount(Animation.INFINITE);
            shake.setRepeatMode(AlphaAnimation.RESTART);
            callAcceptBtn.startAnimation(shake);
        } else {
            textView.setText(String.valueOf(i + 1));
        }

        parent.addView(view);
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        if (loginStatus.isLoggedIn()) {
            getMenuInflater().inflate(R.menu.menu_loggedin, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_home, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        PopUp popUp = new PopUp(this, locationManager, locationListener);
        popUp.popUpMenu(id);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapView = googleMap;
        mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(Key.DefaultCoordinates, 10));
        mapView.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        getLocation();
    }

    public void getLocation() {
        configureLocation();
    }

    public void configureLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);

            } else {
               startLocationService();
            }
        } else {
            startLocationService();
        }
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "app status details " + "ON PAUSE");
        super.onPause();
        active = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationEnabled);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationDisabled);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onlineStatusChanged);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(internetDisconnectedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onlineHrReceiver);

    }

    public void killNotificationsifScreenOn(){
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if(!pm.isScreenOn()){

        } else {
            //All notification will be removed on back pressed
            if(parent.getChildCount() > 0){
                parent.removeAllViews();
            }
            stopMedia();
            MyGcmListenerService.callNotificationArrayList = new ArrayList<>();    // re-initialize details of customer who sent request
            MyGcmListenerService.notificationRequestCount = 0;                     // re-initialize notification count
        }
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "app status details " + "ON RESUME");
        super.onResume();
        active = true;

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("my-event"));
        LocalBroadcastManager.getInstance(this).registerReceiver(locationChangeReceiver,new IntentFilter("locationChange"));
        LocalBroadcastManager.getInstance(this).registerReceiver(locationEnabled,new IntentFilter("locationEnabled"));
        LocalBroadcastManager.getInstance(this).registerReceiver(locationDisabled,new IntentFilter("locationDisabled"));
        LocalBroadcastManager.getInstance(this).registerReceiver(onlineStatusChanged,new IntentFilter("onlineStatusChange"));
        LocalBroadcastManager.getInstance(this).registerReceiver(internetDisconnectedReceiver,new IntentFilter("internetDisconnected"));
        LocalBroadcastManager.getInstance(this).registerReceiver(onlineHrReceiver,new IntentFilter("onlineHr"));
        checkForGPS();
        //Change switch status
        setOnlineSwitch();

        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if(driverProfile != null) {
            Boolean onRide = SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
            Integer rideDriverId = SharedPref.readIntegerFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID);
            if(onRide && driverProfile.getDriverId().equals(rideDriverId)){
                String rideDetailsString = SharedPref.readFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,null);
                    if(rideDetailsString != null){
                        Intent intent = new Intent(this, RideDetails.class);
                        intent.putExtra("rideDetails",rideDetailsString);
                        intent.putExtra("activityRelaunced",true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
            }
        invalidateOptionsMenu();
    }

    public void setDriverOfflineforGPS(){
        SharedPref.saveToPreferences(Home.this, SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_ONLINE,false);
        setOnlineSwitch();
        snackbar = Snackbar.make(fullLayout, getResources().getString(R.string.gpsDisabledText), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Settings", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        snackbar.show();
    }

    public void setOnlineSwitch(){
        if(onlineSwitch != null) {
            Boolean status = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_DRIVER_ONLINE, false);
            onlineSwitch.setOnCheckedChangeListener(null);
            onlineSwitch.setChecked(status);
            onlineSwitch.setOnCheckedChangeListener(this);
            if (status) {
                String text = getResources().getString(R.string.onlineText);
                onlineStatusText.setText(text);
                onlineStatusText.setTextColor(getResources().getColor(R.color.colorDarkGreen));
            } else {
                String text = getResources().getString(R.string.offlineText);
                onlineStatusText.setText(text);
                onlineStatusText.setTextColor(getResources().getColor(R.color.colorDarkRed));
            }
        }
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "app status details " + "ON STOP");
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.accept_btn:
                      //stopMedia();
                int btn_tag = Integer.valueOf(v.getTag().toString());
                //remove all notification views after accepting reques
                      // parent.removeAllViews();
                //loop through all notification list to get right details
                for (int i = 0; i < MyGcmListenerService.callNotificationArrayList.size(); i++) {
                    CallNotification notification = MyGcmListenerService.callNotificationArrayList.get(i);
                    //check if notification id equals tag of button
                    //The view we click on will have same id as decline,accept button tag and notification id
                    if (notification.getNotificationId() == btn_tag) {
                        checkStatusCustomerId = notification.getCustomerId();
                        checkStatusRideId = notification.getRideId();
                        if (notification.getDestinationLocation() != null){
                           checkStatusDestinationLatitude = notification.getDestinationLocation().latitude;
                            checkStatusDestinationLongitude = notification.getDestinationLocation().longitude;
                        }
                        checkAcceptStatus(notification.getNotificationId());
                        break;
                    }
                }
                invalidateOptionsMenu();
                break;


            case R.id.decline_btn:

                int id = Integer.valueOf(v.getTag().toString());               // get tag of button clicked (This tag is same as id of view on which button is clicked
                View view = parent.findViewById(id);                            // get view with same id
                parent.removeView(view);                                        // remove that view


                //loop throught every notification available
                for (int i = 0; i < MyGcmListenerService.callNotificationArrayList.size(); i++) {
                    CallNotification notification = MyGcmListenerService.callNotificationArrayList.get(i);
                    //check if notification id equals tag of button
                    //The view we click on will have same id as decline,accept button tag and notification id
                    if (notification.getNotificationId() == id) {
                        // remove notification details from model
                        MyGcmListenerService.callNotificationArrayList.remove(notification);
                        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
                        if (driverProfile != null) {
                            HashMap<String, Object> params = new HashMap<String, Object>();
                            params.put("driverId", driverProfile.getDriverId());

                            HashMap<String,String> mHeaders = new HashMap<>();
                            mHeaders.put("authToken",driverProfile.getAuthToken());

                            NetworkRequest networkRequest = new NetworkRequest(this, URLS.driverDeclinedURL, Request.Method.POST, TYPEREQUEST.DRIVER_DECLINED, params, mHeaders,true, "",null);
                            networkRequest.setListener(this);
                            networkRequest.getResult();
                        }
                        break;
                    }
                }
                if (parent.getChildCount() == 0) {
                    stopMedia();  //stop media if there is no notification view
                    MyGcmListenerService.callNotificationArrayList = new ArrayList<>();
                    MyGcmListenerService.notificationRequestCount = 0;      //re-initialize notification count as there will be no more request available
                }
                invalidateOptionsMenu();
        }
    }

    public void checkAcceptStatus(final Integer id) {

        final DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if (driverProfile != null) {
            Integer driverID = driverProfile.getDriverId();

            HashMap<String, Object> params = new HashMap<>();
            params.put("customerId", checkStatusCustomerId);
            params.put("rideId", checkStatusRideId);
            params.put("driverId", driverID);

            if (checkStatusDestinationLatitude != null || checkStatusDestinationLongitude != null) {
                params.put("destinationLatitude", checkStatusDestinationLatitude);
                params.put("destinationLongitude", checkStatusDestinationLongitude);
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.checkAcceptStatus, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response != null) {
                            if (response.has(Key.success)) {
                                Integer success = (Integer) response.get(Key.success);
                                if (success == 1) {
                                    // request is accepted
                                    stopMedia();
                                    parent.removeAllViews();
                                   addRideDetailsAndMoveToDetailPage(response);
                                } else {
                                    String message = getResources().getString(R.string.alreadyAcceptedText);
                                    View view = parent.findViewById(id);
                                    parent.removeView(view);

                                    if (parent.getChildCount() == 0) {
                                        invalidateOptionsMenu();
                                        stopMedia();  //stop media if there is no notification view
                                        MyGcmListenerService.callNotificationArrayList = new ArrayList<>();
                                        MyGcmListenerService.notificationRequestCount = 0;      //re-initialize notification count as there will be no more request available
                                    }

                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                    alertDialogBuilder.setTitle(getResources().getString(R.string.alreadyAcceptedTitle));
                                    alertDialogBuilder.setMessage(message);
                                    alertDialogBuilder.setCancelable(true);

                                    alertDialogBuilder.setPositiveButton(getResources().getString(R.string.okText), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                }
                            }
                        } else {

                        }
                    } catch (Exception e) {

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Home.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    stopMedia();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> mHeaders = new HashMap<>();
                    mHeaders.put("authToken", driverProfile.getAuthToken());
                    return mHeaders;
                }
            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getResources().getString(R.string.pleaseWaitText));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public void addRideDetailsAndMoveToDetailPage(JSONObject response) throws JSONException {

        JSONObject details = response.getJSONObject("customerDetails");
        JSONObject user = details.getJSONObject("userId");
        String userFirstName = details.getString("firstName");
        String userLastName = details.getString("lastName");
        String phone = user.getString("contactNo");
        String email = user.optString("email");
        Double rating = details.optDouble("averageRating");
        Double latitude = details.getDouble("latitude");
        Double longitude = details.getDouble("longitude");
        String imageName = details.optString("image");

        JSONObject rangeInfo = response.getJSONObject("extendedDetails");
        Boolean extendedRange = rangeInfo.getBoolean("extendedRange");
        Double extendedRangePrice = rangeInfo.getDouble("extendedRangePrice");

        Customer customer = new Customer();
        customer.setCustomerId(checkStatusCustomerId);
        customer.setFirstName(userFirstName);
        customer.setLastName(userLastName);
        customer.setEmail(email);
        customer.setContactNo(phone);
        customer.setImageName(imageName);
        customer.setRating(rating);

        LatLng customerLocation = new LatLng(latitude,longitude);
        LatLng destinationLocation = null;
        if(checkStatusDestinationLatitude != null && checkStatusDestinationLongitude != null){
            destinationLocation = new LatLng(checkStatusDestinationLatitude,checkStatusDestinationLongitude);
        }

        RideInfo rideInfo = new RideInfo();
        rideInfo.setRideId(checkStatusRideId);
        rideInfo.setCustomer(customer);
        rideInfo.setCustomerLocation(customerLocation);
        rideInfo.setDestinationLocation(destinationLocation);
        rideInfo.setExtendedRange(extendedRange);
        rideInfo.setExtendedRangePrice(extendedRangePrice);

        //re-initialize notification list and count as every notification will be removed after accepting
        MyGcmListenerService.callNotificationArrayList = new ArrayList<>();
        MyGcmListenerService.notificationRequestCount = 0;

        Gson gson = new Gson();
        String rideDetailsString = gson.toJson(rideInfo);
        saveRideDetailsToSharedPref(rideDetailsString);

        Intent rideIntent = new Intent(Home.this, RideDetails.class);
        rideIntent.putExtra("rideDetails",rideDetailsString);
        rideIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(rideIntent);
    }

    public void saveRideDetailsToSharedPref(String rideDetails){
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,true);
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,rideDetails);
        DriverProfile driverProfile = SharedPref.getDriverObject(getApplicationContext(), SharedPrefKey.fileName);
        if(driverProfile != null) {
            SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,driverProfile.getDriverId());
        }
    }


    @Override
    public void onBackPressed() {
        if(parent != null) {
            if(parent.getChildCount() > 0) {

            } else {
                super.onBackPressed();
                //All notification will be removed on back pressed
                MyGcmListenerService.callNotificationArrayList = new ArrayList<>();    // re-initialize details of customer who sent request
                MyGcmListenerService.notificationRequestCount = 0;                     // re-initialize notification count
            }
        }
    }

    public void stopMedia() {
        stopService(new Intent(this, MediaPlayerAndVibratorService.class));
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Integer count = intent.getExtras().getInt("notificationRequestCount");
            String customerId = intent.getExtras().getString("customerId");
            Boolean range = intent.getExtras().getBoolean("extendedRange");

            // To remove duplicate notification if the same customer requests more than once
//            if(MyGcmListenerService.callNotificationArrayList.size() > 1) {
//                // loop through every notification available
//                for (int i = 0; i < MyGcmListenerService.callNotificationArrayList.size(); i++) {
//                    CallNotification notification = MyGcmListenerService.callNotificationArrayList.get(i);
//                    // check if there is request from same customer
//                    if (notification.getCustomerId() == customerId) {
//                        int viewId = notification.getNotificationId();
//                        Log.d("Notification id:","" + notification.getNotificationId());
//                        MyGcmListenerService.callNotificationArrayList.remove(notification);
//                        View view = parent.findViewById(viewId);
//                        parent.removeView(view);
//                        break;
//                    }
//                }
//            }

            //Add Layout including decline and button with tag and id same as that of notification count
            addCallLayout(count,range);
        }
    };

    public BroadcastReceiver locationChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra("location");
            LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
            if (marker == null) {
                mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                marker = mapView.addMarker(new MarkerOptions().title("Current Location").position(latLng).icon(BitmapDescriptorFactory.fromBitmap(createScaledTaxiMarker())));
            } else {
                marker.setPosition(latLng);
            }
        }
    };

    public BroadcastReceiver locationDisabled = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           setDriverOfflineforGPS();
        }
    };

    public BroadcastReceiver locationEnabled = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(snackbar != null) {
                if (snackbar.isShown()) {
                    snackbar.dismiss();
                }
            }
        }
    };

    public BroadcastReceiver onlineStatusChanged = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setOnlineSwitch();
        }
    };
    public BroadcastReceiver internetDisconnectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            SharedPref.saveToPreferences(Home.this, SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_ONLINE,false);
            setOnlineSwitch();
        }
    };

    public BroadcastReceiver onlineHrReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
                String time = intent.getExtras().getString("onlineHrs","");
                onlineHrText.setText(time);
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        startLocationService();
                    } else {
                        configureLocation();
                    }
                } else {
                    configureLocation();
                }

        }
    }


    public void closeApplication(){
        Intent intentHome = new Intent(Intent.ACTION_MAIN);
        intentHome.addCategory(Intent.CATEGORY_HOME);
        intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentHome);
    }

    public void startLocationService(){
        startService(new Intent(Home.this, MyLocationService.class));
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.UPDATE_LOCATION){
            Log.d("Driver Location update:","" + result);
        } else if(typeRequest == TYPEREQUEST.CHANGE_DRIVER_ONLINE_STATUS){
            try{
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1){
                           Boolean status = result.getBoolean("onlineStatus");
                            //Save driver is online or not according to server
                            SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_DRIVER_ONLINE,status);
                            setOnlineSwitch();
                        }else{
                            setOnlineSwitch();
                            String error = result.getString(Key.error);
                            CustomToast.getInstance().showToast(this,error, ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }

            }catch (Exception e){
                Log.d("ExceptionDriver:" ,"" + e.toString());
            }
        } else if(typeRequest == TYPEREQUEST.DRIVER_DECLINED){
            //response when driver declines request
            Log.d("DRIVER DECLINED:","" + result);
        } else if(typeRequest == TYPEREQUEST.ADD_APP_VERSION){
            //response from server
        }
    }


    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.UPDATE_LOCATION) {
           Log.e("Driver Location update:", "" + VolleyErrorMessage.handleVolleyErrors(context, error));
       } else if(typeRequest == TYPEREQUEST.CHANGE_DRIVER_ONLINE_STATUS){
           CustomToast.getInstance().showToast(this,VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.INFO,Toast.LENGTH_SHORT);
       } else if (typeRequest == TYPEREQUEST.DRIVER_DECLINED){
            Log.d("DRIVER DECLINED:","" + VolleyErrorMessage.handleVolleyErrors(context,error));
        }
        setOnlineSwitch();
    }

    private boolean checkForPlayServices(){
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS){
            if(gApi.isUserResolvableError(resultCode)){
                Dialog errorDialog = gApi.getErrorDialog(this,resultCode,1);
                errorDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                        finish();
                    }
                });
                errorDialog.setCancelable(false);
                errorDialog.show();
            } else {
                Toast.makeText(context, getResources().getString(R.string.playserviceErrorText), Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
            if (driverProfile != null) {
                    HashMap<String, Object> params = new HashMap<String, Object>();
                    params.put("driverId", driverProfile.getDriverId());
                    params.put("onlineStatus",isChecked);

                    HashMap<String,String> mHeaders = new HashMap<>();
                    mHeaders.put("authToken",driverProfile.getAuthToken());

                    NetworkRequest networkRequest = new NetworkRequest(this, URLS.changeDriverOnlineStatus, Request.Method.POST, TYPEREQUEST.CHANGE_DRIVER_ONLINE_STATUS, params, mHeaders,false, "Please Wait...",null);
                    networkRequest.setListener(this);
                    networkRequest.getResult();
            }
        } else {
            CustomToast.getInstance().showToast(this,getResources().getString(R.string.enableGPSAlertText), ToastType.INFO,Toast.LENGTH_SHORT);
            setOnlineSwitch();
        }
    }
    public void addAppVersion(){
        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if(driverProfile != null) {
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", driverProfile.getAuthToken());

            HashMap<String,Object> params = new HashMap<>();
            params.put("userId", driverProfile.getUserId());
            params.put("versionName", BuildConfig.VERSION_NAME);
            params.put("versionCode", BuildConfig.VERSION_CODE);
            NetworkRequest networkRequest = new NetworkRequest(this, URLS.addAppVersion, Request.Method.POST, TYPEREQUEST.ADD_APP_VERSION, params, mHeaders,true, "",null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }
    }

    public void checkForGPS() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            setDriverOfflineforGPS();
        }
    }

}

