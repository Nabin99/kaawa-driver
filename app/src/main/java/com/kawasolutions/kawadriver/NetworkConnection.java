package com.kawasolutions.kawadriver;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by apple on 4/26/16.
 */
public class NetworkConnection {

    private Context context;

    public NetworkConnection(Context context){
       this.context = context;
    }

    public boolean isConnected(){
        ConnectivityManager check = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = check.getAllNetworkInfo();

        for (int i = 0; i<info.length; i++){
            if (info[i].getState() == NetworkInfo.State.CONNECTED){
               return true;
            }
        }
        return false;
    }
}
