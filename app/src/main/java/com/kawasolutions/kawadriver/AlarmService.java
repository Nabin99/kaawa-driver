package com.kawasolutions.kawadriver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Utils.SharedPref;

/**
 * Created by apple on 3/6/17.
 */

public class AlarmService extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Latest Android Ver1:","Receiver received broadcast");

        SharedPref.saveToPreferences(context, SharedPrefKey.fileName,SharedPrefKey.SHOW_UPDATE_PAGE,true);
    }
}
