package com.kawasolutions.kawadriver.Constants;

/**
 * Created by apple on 4/22/16.
 */
public class SharedPrefKey {

    public static final String fileName = "KaawaDriver";
    public static final String SENT_TOKEN_TO_SERVER = "";
    public static final String KEY_DRIVER_PROFILE = "driverProfile";
    public static final String KEY_ON_RIDE = "onRide";
    public static final String KEY_RIDE_DETAILS = "rideDetails";
    public static final String KEY_RIDE_DRIVER_ID = "rideDriverId";
    public static final String KEY_RIDE_STARTED = "rideStarted";
    public static final String KEY_RIDE_ENDED = "rideEnded";
    public static final String KEY_RIDE_CANCELLED = "rideCancelled";

    public static final String KEY_RATE_REQUEST = "rateRequest";
    public static final String KEY_RATE_DETAILS = "rateDetails";
    public static final String KEY_RATE_DRIVER_ID = "rateDriverId";
    public static final String KEY_DRIVER_ONLINE = "driverOnline";

    public static final String KEY_CHRONOMETER_TEXT = "chronometerText";
    public static final String KEY_RIDE_DISTANCE = "rideDistanceMeasured";

    public static final String KEY_NEW_APP_UPDATE = "appUpdate";
    public static final String SHOW_UPDATE_PAGE = "updateNow";
    public static final String KEY_NEW_APP_VERSION = "newAppVersion";

    public static final String KEY_METER_PRICE = "meterPrice";

    public static final String KEY_DISTANCE_TIMER_RUNNING = "isdistanceTimerRunning";
}
