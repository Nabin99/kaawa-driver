package com.kawasolutions.kawadriver.Constants;

/**
 * Created by apple on 4/20/16.
 */
public class URLS {
    //test server : 103.198.9.215
        //kawarides.com:8080
    public static final String DOMAIN = "http://103.198.9.215:8080/";
    public static final String MAIN_URL = DOMAIN + "Kaawa/api/",
            logout = MAIN_URL + "driver/logout/",
            loginPost = MAIN_URL  + "login/driver",
            registerPost = MAIN_URL + "register/driver/",
            registerToken = MAIN_URL + "notification/token_registration/",
            updateLocation = MAIN_URL + "driver/update_location/",
            checkAcceptStatus = MAIN_URL + "notification/accept_ride/",
            startRide = MAIN_URL + "ride/start_ride/",
            updateCustomerRating = MAIN_URL + "driver/update_customer_rating",
            rideHistory = MAIN_URL + "driver/driver_last_trips/",
            endride = MAIN_URL + "ride/end_ride",
            rideActivityStatus = MAIN_URL + "ride/ride_activity_status/",
            driverProfile = MAIN_URL + "driver/driver_profile/",
            driverCurrentRide = MAIN_URL + "driver/last_trip/",
            driverAccount = MAIN_URL + "driver/driver_accounts/",
            driverImageURL = DOMAIN + "Kaawa/images/",
            driverDeclinedURL = MAIN_URL + "ride/cancelled_ride_count/",
            saveRequestCount = MAIN_URL + "driver/ride_request_count/",
            changeDriverOnlineStatus = MAIN_URL + "driver/update_online_status/",
            addAppVersion = MAIN_URL + "users/update_app_version/",
            checkAppVersion = MAIN_URL + "version/latest/3" // 3-> for Driver android version update
            ;

}
