package com.kawasolutions.kawadriver.Model;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by apple on 2/26/17.
 */

public class RideInfo {
    private Integer rideId;
    private Customer customer;
    private LatLng customerLocation;
    @Nullable
    private LatLng destinationLocation;
    private Boolean extendedRange;
    private Double extendedRangePrice;

    public Integer getRideId() {
        return rideId;
    }

    public void setRideId(Integer rideId) {
        this.rideId = rideId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LatLng getCustomerLocation() {
        return customerLocation;
    }

    public void setCustomerLocation(LatLng customerLocation) {
        this.customerLocation = customerLocation;
    }

    @Nullable
    public LatLng getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(@Nullable LatLng destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public Boolean getExtendedRange() {
        return extendedRange;
    }

    public void setExtendedRange(Boolean extendedRange) {
        this.extendedRange = extendedRange;
    }

    public Double getExtendedRangePrice() {
        return extendedRangePrice;
    }

    public void setExtendedRangePrice(Double extendedRangePrice) {
        this.extendedRangePrice = extendedRangePrice;
    }
}
