package com.kawasolutions.kawadriver.Model;

import android.support.annotation.Nullable;

/**
 * Created by apple on 5/24/16.
 */
public class RideHistory {

    private String customerName;
    private String date;
    private String status;
    private String customerId;
    private String rideId;
    private String cancelStatus;
    @Nullable
    private String startLocation;
    @Nullable
    private String endLocation;
    @Nullable
    private String imageName;
    @Nullable
    private Double price;
    private Double extendedPrice;


    public RideHistory() {
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    @Nullable
    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(@Nullable String startLocation) {
        this.startLocation = startLocation;
    }

    @Nullable
    public Double getPrice() {
        return price;
    }

    public void setPrice(@Nullable Double price) {
        this.price = price;
    }

    @Nullable
    public String getImageName() {
        return imageName;
    }

    public void setImageName(@Nullable String imageName) {
        this.imageName = imageName;
    }

    @Nullable
    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(@Nullable String endLocation) {
        this.endLocation = endLocation;
    }

    public Double getExtendedPrice() {
        return extendedPrice;
    }

    public void setExtendedPrice(Double extendedPrice) {
        this.extendedPrice = extendedPrice;
    }
}
