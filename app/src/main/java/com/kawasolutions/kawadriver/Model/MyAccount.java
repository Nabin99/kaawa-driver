package com.kawasolutions.kawadriver.Model;

/**
 * Created by apple on 6/29/16.
 */
public class MyAccount {

    private String paymentDate;
    private Double paidAmount;
    private Double totalAmount;
    private Integer rideCount;
    private Double due;

    public MyAccount() {
    }

    public Double getDue() {
        return due;
    }

    public void setDue(Double due) {
        this.due = due;
    }

    public Integer getRideCount() {
        return rideCount;
    }

    public void setRideCount(Integer rideCount) {
        this.rideCount = rideCount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Override
    public String toString() {
        return "MyAccount{" +
                "paymentDate='" + paymentDate + '\'' +
                ", paidAmount=" + paidAmount +
                ", totalAmount=" + totalAmount +
                ", rideCount=" + rideCount +
                ", due=" + due +
                '}';
    }
}
