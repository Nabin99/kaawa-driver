package com.kawasolutions.kawadriver.Model;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by apple on 5/18/16.
 */
public class CallNotification {

    private Integer customerId;
    private Integer rideId;
    @Nullable
    private LatLng destinationLocation;
    private Integer notificationId;
    private Boolean extendedRange;

    public CallNotification() {
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    @Nullable
    public LatLng getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(@Nullable LatLng destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public Integer getRideId() {
        return rideId;
    }

    public void setRideId(Integer rideId) {
        this.rideId = rideId;
    }

    public Boolean getExtendedRange() {
        return extendedRange;
    }

    public void setExtendedRange(Boolean extendedRange) {
        this.extendedRange = extendedRange;
    }
}
