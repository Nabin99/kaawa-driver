/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kawasolutions.kawadriver.GCM;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.kawasolutions.kawadriver.Activities.BulkNotificationActivity;
import com.kawasolutions.kawadriver.Activities.Home;
import com.kawasolutions.kawadriver.Activities.LoginActivity;
import com.kawasolutions.kawadriver.Activities.MainActivity;
import com.kawasolutions.kawadriver.Activities.RideDetails;
import com.kawasolutions.kawadriver.Constants.Key;
import com.kawasolutions.kawadriver.Constants.SharedPrefKey;
import com.kawasolutions.kawadriver.Constants.URLS;
import com.kawasolutions.kawadriver.Enum.TYPEREQUEST;
import com.kawasolutions.kawadriver.LoginStatus;
import com.kawasolutions.kawadriver.Model.CallNotification;
import com.kawasolutions.kawadriver.Model.DriverProfile;
import com.kawasolutions.kawadriver.Model.RideInfo;
import com.kawasolutions.kawadriver.NetworkRequest;
import com.kawasolutions.kawadriver.Service.MediaPlayerAndVibratorService;
import com.kawasolutions.kawadriver.Service.RideInformationService;
import com.kawasolutions.kawadriver.Utils.SharedPref;
import com.kawasolutions.kawadriver.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyGcmListenerService extends GcmListenerService implements NetworkRequest.NetworkListener{

    private static final String TAG = "MyGcmListenerService";
    private NotificationManager notificationManager;
    private Intent mediaIntent;
    private String customerId;
    private String rideId;
    private String message;
    private String type;
    @Nullable
    private String destinationLatitude;
    @Nullable
    private String destinationLongitude;
    @Nullable
    private LatLng destinationLocation;
    public static int notificationRequestCount = 0;
    public static ArrayList<CallNotification> callNotificationArrayList = new ArrayList<CallNotification>();
    private PowerManager.WakeLock wakeLock;
    private Boolean extendedRange;


    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d("Message from GCM:","" + data);
        LoginStatus status = new LoginStatus(this);

        type = data.getString("type");    // gets the type of notification (request or cancellation)

        if (type.equals("request")) {           //if request for taxi
            getDataFromGCM(data);
            if (status.isLoggedIn()) {
                wakeUpDevice();
                saveRequestCount();
                Log.d("Home STatus:", "" + Home.active);
                // if home activity is active (is in Foreground)
                if(Home.active){
                    startService(new Intent(this, MediaPlayerAndVibratorService.class));
                    //add notification details in its model
                    addNotificationData();
                    //broadcast event that new notification is available  (The broadcast Receiver is in Home class)
                    Intent intent = new Intent("my-event");
                    intent.putExtra("notificationRequestCount", notificationRequestCount);
                    intent.putExtra("customerId",customerId);
                    intent.putExtra("extendedRange",extendedRange);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    //increment notification count
                    notificationRequestCount++;
                 }  else {

                    startService(new Intent(this, MediaPlayerAndVibratorService.class));
                    //re-initialize notification count and notification details in list since Home activity is not in foreground state
                    notificationRequestCount = 0;
                    callNotificationArrayList = new ArrayList<>();
                    addNotificationData();

                    //Bundle is passed to Home to know that Home Activity is called because notification has arrived
                    Bundle bundle = new Bundle();
                    bundle.putString("REQUEST", Key.TAXI_REQUEST);
                    bundle.putBoolean("extendedRange",extendedRange);
                    Intent intent = new Intent(this, Home.class);
                    intent.putExtras(bundle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        } else if (type.equals("cancel")) {
            message = data.getString("message");
            Integer rideID = Integer.valueOf(data.getString("rideId"));
            if (status.isLoggedIn()) {
               Boolean rideOn = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
                if(rideOn){
                    String rideDetailsString = SharedPref.readFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
                    if(!rideDetailsString.equals("")){
                        Gson gson = new Gson();
                        RideInfo rideInfo = gson.fromJson(rideDetailsString,RideInfo.class);

                        if(rideInfo != null){
                            if (rideInfo.getRideId().equals(rideID)){
                                //Clear all ride related status

                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DRIVER_ID,0);
                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);
                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,false);
                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,"");
                                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DRIVER_ID,0);

                                stopService(new Intent(this, RideInformationService.class));
                                sendCancelNotification(message);

                                if(RideDetails.active){
                                    Intent intent = new Intent("cancel-event");
                                    intent.putExtra("message", message);
                                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                }

                            } else {
                                Log.d("GCM message:","Ride info id and current ride Id doesnt match");
                            }
                        }else {
                            Log.d("GCM message:","Ride info model null");
                        }
                    } else {
                        Log.d("GCM message:","Ride details null");
                    }
                } else {
                    Log.d("GCM message:","No current Ride to be cancelled");
                }
            }
        } else if (type.equals("bulkNotification")) {
            message = data.getString("message");
            sendBulkNotification(message);
        }

    }

    private void sendBulkNotification(String message) {
        Intent intent = new Intent(this, BulkNotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("message", message);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(getLargeTaxiIcon())
                .setSmallIcon(R.drawable.taxi_marker)
                .setContentTitle("Kawa")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


    private void addNotificationData() {
        CallNotification callNotification = new CallNotification();
        callNotification.setCustomerId(Integer.valueOf(customerId));
        callNotification.setRideId(Integer.valueOf(rideId));
        callNotification.setExtendedRange(extendedRange);
        callNotification.setNotificationId(notificationRequestCount);
        if (destinationLatitude != null && destinationLongitude != null) {
            Log.d("Destination:","" + destinationLatitude + " " + destinationLongitude);
            callNotification.setDestinationLocation(destinationLocation);
        }

        callNotificationArrayList.add(callNotification);
        Log.d("Notification DAta:",""+ callNotificationArrayList);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void sendNotification(String message) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("message", message);

        mediaIntent = new Intent(this, MediaPlayerAndVibratorService.class);

        Intent confirmIntent = new Intent();
        confirmIntent.setAction("Confirm");
        confirmIntent.putExtra("customerId", customerId);
        confirmIntent.putExtra("rideId", rideId);
        if (destinationLatitude != null && destinationLongitude != null) {
            confirmIntent.putExtra("destinationLatitude", destinationLatitude);
            confirmIntent.putExtra("destinationLongitude", destinationLongitude);
        }

        Intent dismissIntent = new Intent();
        dismissIntent.setAction("Dismiss");
        dismissIntent.putExtra("customerId", customerId);
        dismissIntent.putExtra("rideId", rideId);

        Intent clearIntent = new Intent();
        clearIntent.setAction("Clear");
        clearIntent.putExtra("customerId", customerId);
        clearIntent.putExtra("rideId", rideId);

        PendingIntent pendingConfirmIntent = PendingIntent.getBroadcast(this, Integer.valueOf(customerId), confirmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingDismissIntent = PendingIntent.getBroadcast(this, Integer.valueOf(customerId), dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingClearIntent = PendingIntent.getBroadcast(this, Integer.valueOf(customerId), clearIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(getLargeTaxiIcon())
                .setSmallIcon(R.drawable.taxi_marker)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentTitle("Kawa")
                .setContentText(message + "  " + customerId)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setAutoCancel(false)
                .addAction(R.drawable.confirm_icon, "Accept", pendingConfirmIntent)
                .addAction(R.drawable.dismiss_icon, "Decline", pendingDismissIntent)
                .setSound(defaultSoundUri)
                .setDeleteIntent(pendingClearIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .setCategory(Notification.CATEGORY_ALARM);


        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(Integer.valueOf(customerId), notificationBuilder.build());
        startService(mediaIntent);

    }

    private Bitmap getLargeTaxiIcon(){
        return BitmapFactory.decodeResource(getResources(),R.drawable.taxi_marker);
    }

    public void sendCancelNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("message", message);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(getLargeTaxiIcon())
                .setSmallIcon(R.drawable.taxi_marker)
                .setContentTitle("Kawa")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setTicker(message)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }

    public void getDataFromGCM(Bundle data) {
        message = data.getString("message");
        rideId = data.getString("rideId");
        customerId = data.getString("customerId");
        extendedRange = Boolean.valueOf(data.getString("extendedRange"));
        if (data.getString("destinationLatitude") != null && data.getString("destinationLongitude") != null) {
            destinationLatitude = data.getString("destinationLatitude");
            destinationLongitude = data.getString("destinationLongitude");
            destinationLocation = new LatLng(Double.valueOf(destinationLatitude),Double.valueOf(destinationLongitude));
        }
    }

    public void wakeUpDevice() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        if (!pm.isScreenOn()) {
            wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
            wakeLock.acquire();

            KeyguardManager keyguardManager = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
            KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
            keyguardLock.disableKeyguard();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(wakeLock != null) {
            if (wakeLock.isHeld()) {
                wakeLock.release();
            }
        }
    }

    public void saveRequestCount(){
        DriverProfile driverProfile = SharedPref.getDriverObject(this, SharedPrefKey.fileName);
        if (driverProfile != null) {
            HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("driverId", driverProfile.getDriverId());

            HashMap<String,String> mHeaders = new HashMap<>();
            mHeaders.put("authToken",driverProfile.getAuthToken());

            NetworkRequest networkRequest = new NetworkRequest(this, URLS.saveRequestCount, Request.Method.POST, TYPEREQUEST.SAVE_REQUEST_COUNT, params, mHeaders,true, "",null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.SAVE_REQUEST_COUNT){

        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {

    }
}
